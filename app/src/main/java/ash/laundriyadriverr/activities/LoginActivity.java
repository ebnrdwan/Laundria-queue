package ash.laundriyadriverr.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import ash.laundriyadriverr.R;
import ash.laundriyadriverr.models.LoginModel;
import ash.laundriyadriverr.models.UserModel;
import ash.laundriyadriverr.serverconnection.Url;
import ash.laundriyadriverr.serverconnection.volley.AppController;
import ash.laundriyadriverr.serverconnection.volley.ConnectionVolley;
import ash.laundriyadriverr.utils.CheckResponse;
import ash.laundriyadriverr.utils.LoginSharedPreferences;
import ash.laundriyadriverr.utils.Validation.ValidationObject;
import ash.laundriyadriverr.utils.Validation.ViewsValidation;

public class LoginActivity extends Activity implements View.OnClickListener, Response.Listener, Response.ErrorListener {

    EditText mobileEditText, passwordEditText;
    Button loginButton;
    LoginSharedPreferences loginSharedPreferences;
    String requestedUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        initilization();
        set();
    }

    private void initilization(){
        mobileEditText = (EditText) findViewById(R.id.mobileEditText);
        passwordEditText = (EditText) findViewById(R.id.passwordEditText);
        loginButton = (Button) findViewById(R.id.loginButton);
        requestedUrl = "";
    }

    private void set(){
        loginButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.loginButton:
                loginClick();
                break;
        }
    }

    private void loginClick(){
        if (checkValidation()){
            callServerToLogin();
        }
    }

    private void callServerToLogin(){
        requestedUrl = Url.getInstance().loginURL;
        Map<String,String> parms = new HashMap<String,String>();
        UserModel userModel=new UserModel();
        userModel.setUsername(mobileEditText.getText().toString());
        userModel.setPassword(passwordEditText.getText().toString());
        Gson gson = new Gson();
        parms = gson.fromJson(gson.toJson(userModel), parms.getClass());
        Log.e("Parm", parms.toString());
        ConnectionVolley connectionVolley = new ConnectionVolley(this, Request.Method.POST, Url.getInstance().loginURL, this, this, parms, true);
        AppController.getInstance().addToRequestQueue(connectionVolley, "");
    }

    private void callServerToGetOrderList(){
        requestedUrl = Url.getInstance().orderListURL;
        Map<String,String> parms = new HashMap<String,String>();
        parms.put("lat", "30.1");
        parms.put("lng", "31.2");
        Log.e("Parm", parms.toString());
        ConnectionVolley connectionVolley = new ConnectionVolley(this, Request.Method.POST, Url.getInstance().orderListURL, this, this, parms, true);
        AppController.getInstance().addToRequestQueue(connectionVolley, "");
    }

    public boolean checkValidation() {
        ArrayList<ValidationObject> list=new ArrayList<>();
        list.add(new ValidationObject(mobileEditText , 9, true, R.string.mobileErrorMsg));
        list.add(new ValidationObject(passwordEditText , 6, true, R.string.passwordErrorMsg));
        return ViewsValidation.getInstance().validate(this,list);
    }

    private void rememberUser(String jsonString){
        LoginModel loginModel = new LoginModel();
        Gson gson = new Gson();
        loginModel = gson.fromJson(jsonString, LoginModel.class);
        loginSharedPreferences = new LoginSharedPreferences(this, loginModel.getToken(), mobileEditText.getText().toString(), passwordEditText.getText().toString(), "", "", "", "");
    }

    private void toMainActivity(String getJson){
        finish();
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("json", getJson);
        startActivity(intent);
    }

    @Override
    public void onResponse(Object response) {
        Log.e("response", response.toString());
        try {
            if (requestedUrl.equals(Url.getInstance().loginURL)){
                if (CheckResponse.getInstance().checkResponse(this, response.toString(), true)) {
                    rememberUser(response.toString());
                    Log.d("response","success login and remmember");
                    callServerToGetOrderList();
                }
            } else if (requestedUrl.equals(Url.getInstance().orderListURL)){
                if (CheckResponse.getInstance().checkResponse(this, response.toString(), true)) {
                    Log.d("response","get order list");
                    toMainActivity(response.toString());
                }
            }
        } catch (Exception e){
            Toast.makeText(LoginActivity.this, getResources().getString(R.string.connectionError)+": "+e.toString(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        VolleyLog.d("responseError", "Error: " + error.getMessage());
        try {
            ConnectionVolley.dialog.dismiss();
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
