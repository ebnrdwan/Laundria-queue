package ash.laundriyadriverr.activities;

import android.app.Activity;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import ash.laundriyadriverr.R;
import ash.laundriyadriverr.hub.HubCalls;
import ash.laundriyadriverr.models.CategoryModel;
import ash.laundriyadriverr.models.NewOrderModel;
import ash.laundriyadriverr.utils.OrderLocationSharedPreferences;

public class NewOrderActivity extends Activity {

    Button acceptButton, rejectButton;
    TextView idTextView, clientNameTextView, clientPhoneTextView, distanceTextView, addressTextView, categoryNameTextView;
    String json;
    NewOrderModel newOrderModel;
    CategoryModel [] categoryModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_order_activity);


        acceptButton = (Button) findViewById(R.id.acceptButton);
        rejectButton = (Button) findViewById(R.id.rejectButton);
        idTextView = (TextView) findViewById(R.id.idTextView);
        clientNameTextView = (TextView) findViewById(R.id.clientNameTextView);
        clientPhoneTextView = (TextView) findViewById(R.id.clientPhoneTextView);
        distanceTextView = (TextView) findViewById(R.id.distanceTextView);
        addressTextView = (TextView) findViewById(R.id.addressTextView);
        categoryNameTextView = (TextView) findViewById(R.id.categoryNameTextView);
        json = getIntent().getStringExtra("json");
        // LOG1 get the new order from the server
        // TODO: 05/12/2017 error on data from queue
        try {
            decodeJson(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }

        idTextView.setText(getResources().getString(R.string.orderId) +" : "+ newOrderModel.getOrderid());
        clientNameTextView.setText(getResources().getString(R.string.clientName) +" : "+ newOrderModel.getName());
        clientPhoneTextView.setText(getResources().getString(R.string.clientPhone) +" : "+ newOrderModel.getMobileNumber());
        distanceTextView.setText(getResources().getString(R.string.distance) +" : "+ newOrderModel.getDistance());
        //addressTextView.setText(getResources().getString(R.string.address) +" : "+ newOrderModel.);
        for (int i = 0 ; i< categoryModel.length;i++) {
            categoryNameTextView.setText(getResources().getString(R.string.categoryName) + " : " + categoryModel[i].getEnglishName());
        }

        new OrderLocationSharedPreferences(this, newOrderModel.getLat(), newOrderModel.getLng(), newOrderModel.getOrderid(), newOrderModel.getName(), newOrderModel.getMobileNumber(), newOrderModel.getType(), "0");


        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                HubCalls.orderReplay(newOrderModel.getOrderid(), true);
                finish();
                Intent intent = new Intent(NewOrderActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        rejectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                HubCalls.orderReplay(newOrderModel.getOrderid(), false);
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        }, 10000);
    }

    private void decodeJson(String jsonString) throws JSONException {
        Log.i("json String ",""+jsonString);
        Gson gson = new Gson();
        GsonBuilder builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();
        // TODO: 05/12/2017 error in data from Queue
        newOrderModel = gson.fromJson(jsonString, NewOrderModel.class);
        if (newOrderModel==null){
            CategoryModel categoryModel = new CategoryModel("Laundryia",
                    "cloth","Cloth","Cloth","ملابس",
                    "Pickup",false,false,22,33,12,false);
            CategoryModel [] list = {categoryModel,categoryModel};
            newOrderModel =gson.fromJson(Request,NewOrderModel.class);
//            newOrderModel = new NewOrderModel("id","error test","05573","33.3km",list,"30.0677892",)
        }
        Log.i("newOrderModel",""+newOrderModel);
        gson = new Gson();
        builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();
        Log.i("getCategory",""+newOrderModel.getCategory());

//       Type listType = new TypeToken<List<CategoryModel>>() {}.getType();
//        List<CategoryModel> posts = new Gson().fromJson(newOrderModel.getCategory().toString(), listType);
        categoryModel = newOrderModel.getCategory();
        //  categoryModel = gson.fromJson(builder.create().toJson(newOrderModel.getCategory()), CategoryModel.class);
    }


    String Request ="{\n" +
            "                                                                   \"type\": 1,\n" +
            "                                                                   \"Orderid\": 10174,\n" +
            "                                                                   \"Name\": \"mmm \",\n" +
            "                                                                   \"MobileNumber\": \"552369874\",\n" +
            "                                                                   \"distance\": 0.0,\n" +
            "                                                                   \"Category\": [\n" +
            "                                                                     {\n" +
            "                                                                       \"Company\": null,\n" +
            "                                                                       \"Items\": null,\n" +
            "                                                                       \"ParentCategory\": null,\n" +
            "                                                                       \"EnglishName\": \"Clothes\",\n" +
            "                                                                       \"ArabicName\": \"ملابس\",\n" +
            "                                                                       \"ParentCategoryId\": null,\n" +
            "                                                                       \"EnableNow\": false,\n" +
            "                                                                       \"DirectToAdmin\": false,\n" +
            "                                                                       \"MinCharge\": 12,\n" +
            "                                                                       \"Id\": 43,\n" +
            "                                                                       \"CompanyId\": 2,\n" +
            "                                                                       \"CreationDate\": \"2017-12-02T13:14:00.93\",\n" +
            "                                                                       \"LastUpdateDate\": \"2017-12-02T13:14:00.93\",\n" +
            "                                                                       \"IsDeleted\": false\n" +
            "                                                                     }\n" +
            "                                                                   ],\n" +
            "                                                                   \"Lat\": 30.0743584,\n" +
            "                                                                   \"Lng\": 31.34480679999999\n" +
            "                                                                 }";
}
