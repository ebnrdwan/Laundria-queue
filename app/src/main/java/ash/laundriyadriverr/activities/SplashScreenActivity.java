package ash.laundriyadriverr.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import ash.laundriyadriverr.R;
import ash.laundriyadriverr.models.LoginModel;
import ash.laundriyadriverr.models.UserModel;
import ash.laundriyadriverr.serverconnection.Url;
import ash.laundriyadriverr.serverconnection.volley.AppController;
import ash.laundriyadriverr.serverconnection.volley.ConnectionVolley;
import ash.laundriyadriverr.utils.CheckResponse;
import ash.laundriyadriverr.utils.LoginSharedPreferences;

public class SplashScreenActivity extends Activity implements Response.Listener, Response.ErrorListener {

    Handler handler;
    LoginSharedPreferences loginSharedPreferences;
    String requestedUrl;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen_activity);
        initialization();
        checkSharedPreference();
        intent = getIntent();
    }

    private void initialization() {
        handler = new Handler();
        loginSharedPreferences = new LoginSharedPreferences();
        requestedUrl = "";
    }

    private void checkSharedPreference() {
        if (!loginSharedPreferences.getAccessToken(this).equals("")) {
            callServerToLogin();
        } else {
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    toLogin();
                }
            }, 2000);
        }
    }

    private void callServerToLogin() {
        requestedUrl = Url.getInstance().loginURL;
        Map<String, String> parms = new HashMap<String, String>();
        UserModel userModel = new UserModel();
        userModel.setUsername(loginSharedPreferences.getPhoneNumber(this));
        userModel.setPassword(loginSharedPreferences.getPassword(this));
        Gson gson = new Gson();
        parms = gson.fromJson(gson.toJson(userModel), parms.getClass());
        Log.e("Parm", parms.toString());
        ConnectionVolley connectionVolley = new ConnectionVolley(this, Request.Method.POST, Url.getInstance().loginURL, this, this, parms, false);
        AppController.getInstance().addToRequestQueue(connectionVolley, "");
    }

    private void callServerToGetOrderList() {
        requestedUrl = Url.getInstance().orderListURL;
        Map<String, String> parms = new HashMap<String, String>();
        parms.put("lat", "30.1");
        parms.put("lng", "31.2");
        Log.e("Parm", parms.toString());
        ConnectionVolley connectionVolley = new ConnectionVolley(this, Request.Method.POST, Url.getInstance().orderListURL, this, this, parms, true);
        AppController.getInstance().addToRequestQueue(connectionVolley, "");
    }


    void callServerToGetCategories() {
        Map<String, String> parms = new HashMap<String, String>();
        ConnectionVolley connectionVolley = new ConnectionVolley(this, Request.Method.POST, Url.getInstance().categoriesURL, this, this, parms, false);
        AppController.getInstance().addToRequestQueue(connectionVolley, "");
    }

    void callServerToShowLocationRequests() {
        Map<String, String> parms = new HashMap<String, String>();
        ConnectionVolley connectionVolley = new ConnectionVolley(this, Request.Method.POST, Url.getInstance().showLocationRequestsURL, this, this, parms, false);
        AppController.getInstance().addToRequestQueue(connectionVolley, "");
    }

    private void rememberUser(String jsonString) {
        LoginModel loginModel = new LoginModel();
        Gson gson = new Gson();
        loginModel = gson.fromJson(jsonString, LoginModel.class);
        loginSharedPreferences = new LoginSharedPreferences(this, loginModel.getToken(), loginSharedPreferences.getPhoneNumber(this), loginSharedPreferences.getPassword(this), "", "", "", "");
    }

    private void toLogin() {
        finish();
        Intent toHome = new Intent(this, LoginActivity.class);
        startActivity(toHome);
    }

    private void toMainActivity(String jsonString) {
        finish();

        Intent toMainActivity = new Intent(this, MainActivity.class);
        if (intent.getExtras()!=null) {
            String action = intent.getExtras().getString("choose");
            if (action!=null){
                if (action.equals("choose")) {
                    toMainActivity.putExtra("choose", "choose");
                }
            }

        }

        toMainActivity.putExtra("json", jsonString);
        startActivity(toMainActivity);
    }

    @Override
    public void onResponse(Object response) {
        Log.e("response", response.toString());

            if (requestedUrl.equals(Url.getInstance().loginURL)) {
                if (CheckResponse.getInstance().checkResponse(this, response.toString(), true)) {
                    rememberUser(response.toString());
                    callServerToGetOrderList();
                }
            } else if (requestedUrl.equals(Url.getInstance().orderListURL)) {
                if (CheckResponse.getInstance().checkResponse(this, response.toString(), true)) {
                    toMainActivity(response.toString());
                }
            }

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        VolleyLog.d("responseError", "Error: " + error.getMessage());

        try {
            ConnectionVolley.dialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}