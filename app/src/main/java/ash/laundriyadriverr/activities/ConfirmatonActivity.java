package ash.laundriyadriverr.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import ash.laundriyadriverr.R;
import ash.laundriyadriverr.hub.HubCalls;
import ash.laundriyadriverr.utils.OrderLocationSharedPreferences;

public class ConfirmatonActivity extends Activity {

    OrderLocationSharedPreferences orderLocationSharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_confirmaton);
        orderLocationSharedPreferences = new OrderLocationSharedPreferences();

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_confirmaton);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Button yesButton = (Button) dialog.findViewById(R.id.yesButton);
        Button noButton = (Button) dialog.findViewById(R.id.noButton);
        yesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HubCalls.moneyConfirm(orderLocationSharedPreferences.getOrderId(ConfirmatonActivity.this), ConfirmatonActivity.this);
                Toast.makeText(ConfirmatonActivity.this, getResources().getString(R.string.done), Toast.LENGTH_LONG).show();
                dialog.dismiss();
//                showChooseDialog();
                finish();
            }
        });
        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HubCalls.drivercancelorder(orderLocationSharedPreferences.getOrderId(ConfirmatonActivity.this), ConfirmatonActivity.this);
                Toast.makeText(ConfirmatonActivity.this, getResources().getString(R.string.done), Toast.LENGTH_LONG).show();
                finish();
            }
        });
        dialog.show();

    }
}
