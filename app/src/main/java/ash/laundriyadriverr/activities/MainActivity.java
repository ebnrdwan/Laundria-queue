package ash.laundriyadriverr.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

import ash.laundriyadriverr.R;
import ash.laundriyadriverr.fragments.HomeMapFragment;
import ash.laundriyadriverr.hub.HubCalls;
import ash.laundriyadriverr.hub.HubFactory;
import ash.laundriyadriverr.models.MainMenuModel;
import ash.laundriyadriverr.utils.GeneralClass;
import ash.laundriyadriverr.utils.LoginSharedPreferences;
import ash.laundriyadriverr.utils.OrderLocationSharedPreferences;
import ash.laundriyadriverr.utils.Permissions;
import microsoft.aspnet.signalr.client.hubs.HubProxy;

public class MainActivity extends FragmentActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    ImageView menuImageView;
    Toolbar toolbar;
    DrawerLayout drawerLayout;
    ListView menuListView;
    ArrayList<MainMenuModel> mainMenuModels;
    MainMenuAdapter mainMenuAdapter;
    LoginSharedPreferences loginSharedPreferences;
    Permissions permissions;
    String getJson;
    TextView statusTextView;
    OrderLocationSharedPreferences orderLocationSharedPreferences;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialization();
        menuContent();
        set();
        callAdapter();
        homeFragment();
        intent = getIntent();
        if (intent.getAction() != null) {
            if (intent.getAction().equals("queue")) {

                HubCalls.getItemsFromQueue(MainActivity.this);
            } else if (intent.getAction().equals("station")) {
                HomeMapFragment.toStation = true;
                homeFragment();
                Toast.makeText(MainActivity.this, "goto station", Toast.LENGTH_LONG).show();
            } else if ((intent.getExtras().getString("choose").equals("choose"))) {
                showChooseDialog();
            } else {
                HomeMapFragment.toStation = false;
            }
        }


        if (orderLocationSharedPreferences.getOrderLat(this).equals("0")) {
            statusTextView.setText(getString(R.string.free));
        } else {
            statusTextView.setText(getString(R.string.busy));
        }

        new SetupTheHub().execute();
    }

    public void showChooseDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.choose_end_behavior);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Button proceedButton = (Button) dialog.findViewById(R.id.proceedButton);
        Button endButton = (Button) dialog.findViewById(R.id.endButton);
        dialog.show();
        proceedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                HubCalls.moneyConfirm(orderLocationSharedPreferences.getOrderId(ConfirmatonActivity.this), ConfirmatonActivity.this);


                HubCalls.getItemsFromQueue(MainActivity.this);
//                HubCalls.PostOrder(ConfirmatonActivity.this);
                Intent intent = new Intent(MainActivity.this, MainActivity.class).setAction("queue");
                startActivity(intent);
                Toast.makeText(MainActivity.this, getResources().getString(R.string.done), Toast.LENGTH_LONG).show();
                // TODO: 19/11/2017 check to whether fetch new request or end day
//                EditOrderFragment fragment = new EditOrderFragment();
//                fragment.callServerToGetOrder();
                dialog.dismiss();

            }
        });
        endButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, MainActivity.class).setAction("station");
                startActivity(intent);
                Toast.makeText(MainActivity.this, getResources().getString(R.string.done), Toast.LENGTH_LONG).show();
                finish();
            }
        });
    }

    private void homeFragment() {
        HomeMapFragment homeFragment = new HomeMapFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.contentView, homeFragment).commit();
    }


    public class SetupTheHub extends AsyncTask<Void, String, HubProxy> {
        HubFactory hubFactory;

        @Override
        protected HubProxy doInBackground(Void... params) {
            hubFactory = HubFactory.getInstance(MainActivity.this, loginSharedPreferences.getAccessToken(MainActivity.this));
            hubFactory.forceStop = false;
            return hubFactory.getTheHub();
        }

        @Override
        protected void onPostExecute(HubProxy hubProxy) {
            //  updateTitleConnection(HubFactory.connection.getState());
        }
    }   //

    private void initialization() {
        menuImageView = (ImageView) findViewById(R.id.menuImageView);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        menuListView = (ListView) findViewById(R.id.menuListView);
        statusTextView = (TextView) findViewById(R.id.statusTextView);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        mainMenuModels = new ArrayList<>();
        loginSharedPreferences = new LoginSharedPreferences();
        permissions = new Permissions();
        getJson = getIntent().getStringExtra("json");
        orderLocationSharedPreferences = new OrderLocationSharedPreferences();
    }

    private void callAdapter() {
        mainMenuAdapter = new MainMenuAdapter(this, mainMenuModels);
        menuListView.setAdapter(mainMenuAdapter);
    }

    private void set() {
        menuImageView.setOnClickListener(this);
        menuListView.setOnItemClickListener(this);
    }

    private void menuContent() {
        mainMenuModels.add(new MainMenuModel(getResources().getString(R.string.shareApp), R.mipmap.car_icon));
        mainMenuModels.add(new MainMenuModel(getResources().getString(R.string.language), R.mipmap.car_icon));
        mainMenuModels.add(new MainMenuModel(getResources().getString(R.string.signOut), R.mipmap.car_icon));
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        drawerLayout.closeDrawers();
        switch (i) {
            case 0:
                shareApp();
                break;
            case 1:
                checkkLanguage();
                break;
            case 2:
                signOut();
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.menuImageView:
                openAndCloseDrawer();
                break;
        }
    }

    private void openAndCloseDrawer() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawers();
        } else {
            drawerLayout.openDrawer(GravityCompat.START);
        }
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(menuListView)) {
            drawerLayout.closeDrawer(menuListView);
        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() != 0) {
                FragmentManager.BackStackEntry backEntry = getSupportFragmentManager().getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount() - 1);
                getSupportFragmentManager().popBackStack();
                getSupportFragmentManager().beginTransaction().remove(getSupportFragmentManager().findFragmentById(R.id.contentView)).commit();
            } else {
                finish();
            }
        }
    }

    public void checkkLanguage() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("language", 0);
        if (pref.getString("language", "en").equals("en")) {
            checkLanguage("ar");
        } else {
            checkLanguage("en");
        }
    }

    public void checkLanguage(String languageToLoad) {
        Locale locale = new Locale(languageToLoad);
        rememberLanguage(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getApplicationContext().getResources().updateConfiguration(config, getApplicationContext().getResources().getDisplayMetrics());
    }

    private void rememberLanguage(String language) {
        GeneralClass.language = language;
        SharedPreferences pref = getApplicationContext().getSharedPreferences("language", 0);
        SharedPreferences.Editor ed = pref.edit();
        ed.putString("language", language);
        ed.commit();
        restartActivity();
    }

    private void restartActivity() {
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }

    private void shareApp() {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = "Here is the share content body";
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.shareVia)));
    }

    private void signOut() {
        loginSharedPreferences.removeLogin(this);
        finish();
        Intent toHomeActivity = new Intent(this, LoginActivity.class);
        startActivity(toHomeActivity);
    }

    public class MainMenuAdapter extends BaseAdapter {

        Activity activity;
        ArrayList<MainMenuModel> mainMenuModels;
        ViewHolder holder;

        public MainMenuAdapter() {
        }

        public MainMenuAdapter(Activity activity, ArrayList<MainMenuModel> mainMenuModels) {
            this.activity = activity;
            this.mainMenuModels = mainMenuModels;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            convertView = activity.getLayoutInflater().inflate(R.layout.menu_list_custom, parent, false);
            holder = new ViewHolder();
            holder.menuItemIconImageView = (ImageView) convertView.findViewById(R.id.menuItemIconImageView);
            holder.menuItemNameTextView = (TextView) convertView.findViewById(R.id.menuItemNameTextView);

            holder.menuItemNameTextView.setText(mainMenuModels.get(position).getMenuName());
            holder.menuItemIconImageView.setImageResource(mainMenuModels.get(position).getMenuIcon());

            return convertView;
        }

        public int getCount() {
            return mainMenuModels.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        private class ViewHolder {
            private ImageView menuItemIconImageView;
            private TextView menuItemNameTextView;
        }
    }

}