package ash.laundriyadriverr.hub;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import ash.laundriyadriverr.R;
import ash.laundriyadriverr.services.MyReceiver;
import ash.laundriyadriverr.services.MyReceiverConfirmation;
import ash.laundriyadriverr.utils.GeneralClass;
import microsoft.aspnet.signalr.client.Action;
import microsoft.aspnet.signalr.client.ConnectionState;
import microsoft.aspnet.signalr.client.ErrorCallback;
import microsoft.aspnet.signalr.client.hubs.SubscriptionHandler1;

public class HubCalls {
    static Context mContext;
    private final static String HUB_TAG = "HubCalls";

    public HubCalls(Context mContext) {
        this.mContext = mContext;
    }

    public void DriverCannllReply(final Context context, int CallId, boolean accept, String userNumber) {
        try {
            if (HubFactory.connection.getState() == ConnectionState.Connected && HubFactory.hub != null) {
                HubFactory.hub.invoke("DriverCallReply", "x30.073577", "31.3161993").done(new Action<Void>() {
                    @Override
                    public void run(Void aVoid) throws Exception {
                        Log.v(HUB_TAG, "ImConnected Ok");
                    }
                }).onError(new ErrorCallback() {
                    @Override
                    public void onError(Throwable throwable) {
                        Log.v(HUB_TAG, " driver call reply ImConnected Error");
                        BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
                    }
                });
            } else {
                BroadcastHelper.sendInform(BroadcastHelper.DRIVER_CALLREPLY_BROADCAST_METHOD, context);
            }
        } catch (Exception e) {
            e.printStackTrace();
            BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
        }
    }

    public static void getStationLocation(final Context context) {
        try {
            if (HubFactory.connection.getState() == ConnectionState.Connected && HubFactory.hub != null) {
                HubFactory.hub.invoke("getLocationofDriverBranch").done(new Action<Void>() {
                    @Override
                    public void run(Void aVoid) throws Exception {
                        Log.v(HUB_TAG, "station get location");
                    }
                }).onError(new ErrorCallback() {
                    @Override
                    public void onError(Throwable throwable) {
                        Log.v(HUB_TAG, "error getting station location");
                        BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
                    }
                });
                HubFactory.hub.on("Branchlocation", new SubscriptionHandler1<String>() {
                    @Override
                    public void run(String s) {
                        Log.v(HUB_TAG, "station location: " + s.toString());
                        Toast.makeText(context, "your branch location is :" + s, Toast.LENGTH_SHORT).show();


                    }

                }, String.class);
            } else {
                BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
            }
        } catch (Exception e) {
            e.printStackTrace();
            BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
        }
    }

    public static void updateDriverLocation(final Context context) {
        try {
            if (HubFactory.connection.getState() == ConnectionState.Connected && HubFactory.hub != null) {
                HubFactory.hub.invoke("updateLocation", "30.073577", "31.3161993").done(new Action<Void>() {
                    @Override
                    public void run(Void aVoid) throws Exception {
                        Log.v(HUB_TAG, "update driver location ImConnected Ok");
                    }
                }).onError(new ErrorCallback() {
                    @Override
                    public void onError(Throwable throwable) {
                        Log.v(HUB_TAG, "update location ImConnected Error");
                        BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
                    }
                });
            } else {
                BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
            }
        } catch (Exception e) {
            e.printStackTrace();
            BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
        }
    }
    // updateDriverLocation()

    public static void updateLocation(final String lat, final String lng) {

        try {
            if (HubFactory.connection.getState() == ConnectionState.Connected && HubFactory.hub != null) {

                // invoke(String.class, "iAmAvailable", "username", "password", "TransMedic")
                HubFactory.hub.invoke("updateLocation", lat, lng).done(new Action<Void>() {
                    @Override
                    public void run(Void aVoid) throws Exception {
                        Log.v(HUB_TAG, "location updated successfully");
                    }
                }).onError(new ErrorCallback() {
                    @Override
                    public void onError(Throwable throwable) {
                        Log.v(HUB_TAG, throwable.toString());
                        //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
                    }
                });

            } else {
                //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
            }

        } catch (Exception e) {
            e.printStackTrace();
            //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
        }
    }

    public static void updateOrder(final Context context, String id) {
        try {
            if (HubFactory.connection.getState() == ConnectionState.Connected && HubFactory.hub != null) {
                Log.v("djadbjfkaabfbdbfuufb", "afdafafdnuj");
                // invoke(String.class, "iAmAvailable", "username", "password", "TransMedic")
                HubFactory.hub.invoke("UpdateOrder", id).done(new Action<Void>() {
                    @Override
                    public void run(Void aVoid) throws Exception {
                        Log.v(HUB_TAG, "updated order success");
                    }
                }).onError(new ErrorCallback() {
                    @Override
                    public void onError(Throwable throwable) {
                        Log.v(HUB_TAG, "updated order failed");
                        //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
                    }
                });

                HubFactory.hub.on("UpdatedDone", new SubscriptionHandler1<String>() {
                    @Override
                    public void run(String msg) {
                        Log.e("result :::= ", msg);
                        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                                .setSmallIcon(R.mipmap.logo)
                                .setContentTitle(context.getResources().getString(R.string.app_name))
                                .setContentText("Done")
                                .setAutoCancel(true)
                                .setSound(defaultSoundUri)
                                .setContentIntent(null);
                        NotificationManager notificationManager =
                                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
                    }
                }, String.class);

                HubFactory.hub.on("finishservice", new SubscriptionHandler1<String>() {
                    @Override
                    public void run(String msg) {
                        Log.e("result :::= ", msg);
                        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                                .setSmallIcon(R.mipmap.logo)
                                .setContentTitle(context.getResources().getString(R.string.app_name))
                                .setContentText("Finish service")
                                .setAutoCancel(true)
                                .setSound(defaultSoundUri)
                                .setContentIntent(null);
                        NotificationManager notificationManager =
                                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
                    }
                }, String.class);

                HubFactory.hub.on("ClientConfirm", new SubscriptionHandler1<String>() {
                    @Override
                    public void run(String msg) {
                        Log.e("result :::= ", msg);
                        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                                .setSmallIcon(R.mipmap.logo)
                                .setContentTitle(context.getResources().getString(R.string.app_name))
                                .setContentText("Client Confirm")
                                .setAutoCancel(true)
                                .setSound(defaultSoundUri)
                                .setContentIntent(null);
                        NotificationManager notificationManager =
                                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());

                        Intent intent = new Intent(context, MyReceiverConfirmation.class);
                        context.sendBroadcast(intent);

                    }
                }, String.class);
            } else {
                //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
            }

        } catch (Exception e) {
            e.printStackTrace();
            //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
        }
    }

    public static void orderReplay(String orderId, boolean b) {
        Log.v("rrrrrrrrrrrrrr", "afdafafdnuj");
        try {
            if (HubFactory.connection.getState() == ConnectionState.Connected && HubFactory.hub != null) {
                // invoke(String.class, "iAmAvailable", "username", "password", "TransMedic")
                HubFactory.hub.invoke("orderReplay", orderId, b).done(new Action<Void>() {
                    @Override
                    public void run(Void aVoid) throws Exception {
                        Log.v(HUB_TAG, "orderReplay");
                    }
                }).onError(new ErrorCallback() {
                    @Override
                    public void onError(Throwable throwable) {
                        Log.v(HUB_TAG, "order reply failed");
                        //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
                    }
                });

                HubFactory.hub.on("hi", new SubscriptionHandler1<String>() {
                    @Override
                    public void run(String msg) {
                        Log.e("HI :::= ", msg);
                    }
                }, String.class);
            } else {
                //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
            }

        } catch (Exception e) {
            e.printStackTrace();
            //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
        }
    }

    public static void driverComing(String id, final Context context) {
        Log.v("rrrrrrrrrrrrrr", "afdafafdnuj");
        try {
            if (HubFactory.connection.getState() == ConnectionState.Connected && HubFactory.hub != null) {
                Log.v("djadbjfkaabfbdbfuufb", "afdafafdnuj");
                // invoke(String.class, "iAmAvailable", "username", "password", "TransMedic")
                HubFactory.hub.invoke("DriverComing", id).done(new Action<Void>() {
                    @Override
                    public void run(Void aVoid) throws Exception {
                        Log.v(HUB_TAG, "orderReplay");
                    }
                }).onError(new ErrorCallback() {
                    @Override
                    public void onError(Throwable throwable) {
                        Log.v(HUB_TAG, " driver comming ImConnected Error");
                        //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
                    }
                });

                HubFactory.hub.on("ClientConfirm", new SubscriptionHandler1<String>() {
                    @Override
                    public void run(String msg) {
                        Log.e("HI :::= ", msg);
                        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                                .setSmallIcon(R.mipmap.logo)
                                .setContentTitle(context.getResources().getString(R.string.app_name))
                                .setContentText(context.getResources().getString(R.string.orderId) + " : " + msg + " " + context.getString(R.string.accept))
                                .setAutoCancel(true)
                                .setSound(defaultSoundUri)
                                .setContentIntent(null);
                        NotificationManager notificationManager =
                                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
                    }
                }, String.class);
            } else {
                //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
            }

        } catch (Exception e) {
            e.printStackTrace();
            //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
        }
    }

    public static void itemsToLaundry(String id, final Context context) {
        Log.v("rrrrrrrrrrrrrr", "afdafafdnuj");
        try {
            if (HubFactory.connection.getState() == ConnectionState.Connected && HubFactory.hub != null) {
                Log.v("djadbjfkaabfbdbfuufb", "afdafafdnuj");
                // invoke(String.class, "iAmAvailable", "username", "password", "TransMedic")
                HubFactory.hub.invoke("itemsToLaundry", id).done(new Action<Void>() {
                    @Override
                    public void run(Void aVoid) throws Exception {
                        Log.v(HUB_TAG, "orderReplay");
                    }
                }).onError(new ErrorCallback() {
                    @Override
                    public void onError(Throwable throwable) {
                        Log.v(HUB_TAG, " to laundriya ImConnected Error");
                        //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
                    }
                });
            } else {
                //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
            }

        } catch (Exception e) {
            e.printStackTrace();
            //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
        }
    }


    public static void getOrdersFromqueue(final Context context) {
        Log.v("rrrrrrrrrrrrrr", "afdafafdnuj");
        try {
            if (HubFactory.connection.getState() == ConnectionState.Connected && HubFactory.hub != null) {
                Log.v("djadbjfkaabfbdbfuufb", "afdafafdnuj");
                // invoke(String.class, "iAmAvailable", "username", "password", "TransMedic")
                HubFactory.hub.invoke("getOrdersfromQueues").done(new Action<Void>() {
                    @Override
                    public void run(Void aVoid) throws Exception {
                        Log.v(HUB_TAG, "orderReplay");
                    }
                }).onError(new ErrorCallback() {
                    @Override
                    public void onError(Throwable throwable) {
                        Log.v(HUB_TAG, "get orders from queue ImConnected Error");
                        //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
                    }
                });
            } else {
                //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
            }

        } catch (Exception e) {
            e.printStackTrace();
            //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
        }
    }

    public static void getItemsFromQueue(final Context context) {

        try {
            if (HubFactory.connection.getState() == ConnectionState.Connected && HubFactory.hub != null) {
                HubFactory.hub.invoke("getOrdersfromQueues").done(new Action<Void>() {
                    @Override
                    public void run(Void aVoid) throws Exception {
                        Log.v(HUB_TAG, "orders from queue");
                        BroadcastHelper.sendInform(BroadcastHelper.SUCCESS_POST_ORDER_SEND_BROADCAST_METHOD, context);
                    }
                }).onError(new ErrorCallback() {
                    @Override
                    public void onError(Throwable throwable) {
                        Log.v(HUB_TAG, "ok Error");
                        BroadcastHelper.sendInform(BroadcastHelper.ERROR_POST_ORDER_SEND_BROADCAST_METHOD, context);
                    }
                });

                HubFactory.hub.on("broadcastMessage", new SubscriptionHandler1<String>() {
                    @Override
                    public void run(String msg) {
                        Log.d(HUB_TAG, msg);
                    }
                }, String.class);
            } else {
                BroadcastHelper.sendInform(BroadcastHelper.ERROR_POST_ORDER_SEND_BROADCAST_METHOD, context);
            }
        } catch (Exception e) {
            e.printStackTrace();
            BroadcastHelper.sendInform(BroadcastHelper.ERROR_POST_ORDER_SEND_BROADCAST_METHOD, context);

        }
    }

    public static void moneyConfirm(String id, final Context context) {
        Log.v("rrrrrrrrrrrrrr", "afdafafdnuj");
        try {
            if (HubFactory.connection.getState() == ConnectionState.Connected && HubFactory.hub != null) {
                if (GeneralClass.money == 0) {
                    GeneralClass.money = 1;
                    Log.v("djadbjfkaabfbdbfuufb", "afdafafdnuj");
                    // invoke(String.class, "iAmAvailable", "username", "password", "TransMedic")
                    HubFactory.hub.invoke("MoneyConfirm", id).done(new Action<Void>() {
                        @Override
                        public void run(Void aVoid) throws Exception {
                            Log.v(HUB_TAG, "MoneyConfirm");
                        }
                    }).onError(new ErrorCallback() {
                        @Override
                        public void onError(Throwable throwable) {
                            Log.v(HUB_TAG, " money confirm ImConnected Error");
                            //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
                        }
                    });
                }

            } else {
                //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
            }

        } catch (Exception e) {
            e.printStackTrace();
            //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
        }
    }

    public static void drivercancelorder(String id, final Context context) {
        Log.v("rrrrrrrrrrrrrr", "afdafafdnuj");
        try {
            if (HubFactory.connection.getState() == ConnectionState.Connected && HubFactory.hub != null) {
                Log.v("djadbjfkaabfbdbfuufb", "afdafafdnuj");
                // invoke(String.class, "iAmAvailable", "username", "password", "TransMedic")
                HubFactory.hub.invoke("drivercancelorder", id).done(new Action<Void>() {
                    @Override
                    public void run(Void aVoid) throws Exception {
                        Log.v(HUB_TAG, "drivercancelorder");
                    }
                }).onError(new ErrorCallback() {
                    @Override
                    public void onError(Throwable throwable) {
                        Log.v(HUB_TAG, "driver cancel ImConnected Error");
                        //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
                    }
                });
            } else {
                //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
            }

        } catch (Exception e) {
            e.printStackTrace();
            //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
        }
    }


    public static void DriverHere(String id) {
        Log.v("rrrrrrrrrrrrrr", "afdafafdnuj");
        try {
            if (HubFactory.connection.getState() == ConnectionState.Connected && HubFactory.hub != null) {
                Log.v("djadbjfkaabfbdbfuufb", "afdafafdnuj");
                // invoke(String.class, "iAmAvailable", "username", "password", "TransMedic")
                HubFactory.hub.invoke("DriverHere", id).done(new Action<Void>() {
                    @Override
                    public void run(Void aVoid) throws Exception {
                        Log.v(HUB_TAG, "ImHere");
                    }
                }).onError(new ErrorCallback() {
                    @Override
                    public void onError(Throwable throwable) {
                        Log.v(HUB_TAG, "driver here ImConnected Error");
                        //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
                    }
                });

                HubFactory.hub.on("ClientConfirm", new SubscriptionHandler1<String>() {
                    @Override
                    public void run(String msg) {
                        Log.e("ClientConfirm :::= ", msg);
                    }
                }, String.class);
            } else {
                //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
            }

        } catch (Exception e) {
            e.printStackTrace();
            //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
        }
    }

    public static void clientConnected(final Context context) {
        try {
            if (HubFactory.connection.getState() == ConnectionState.Connected && HubFactory.hub != null) {
                // invoke(String.class, "iAmAvailable", "username", "password", "TransMedic")
                if (GeneralClass.hubd == 0) {
                    HubFactory.hub.invoke(String.class, "driverConnected").done(new Action<String>() {
                        @Override
                        public void run(String s) throws Exception {
                            Log.v(HUB_TAG, "driver connected" + s);
                            Toast.makeText(context, "driver connected", Toast.LENGTH_SHORT).show();
                            GeneralClass.hubd = 1;
                        }
                    }).onError(new ErrorCallback() {
                        @Override
                        public void onError(Throwable throwable) {
                            Log.v(HUB_TAG, "driver connecting error:" + throwable.toString());
                            BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
                        }
                    });

                    HubFactory.hub.on("Neworder", new SubscriptionHandler1<String>() {
                        @Override
                        public void run(String json) {
                            Log.e("Neworder1", json);
                            Intent intent = new Intent(context, MyReceiver.class);
                            Bundle extras = new Bundle();
                            extras.putString("json", json);
                            intent.putExtras(extras);
                            context.sendBroadcast(intent);
                        }
                    }, String.class);
                }

            } else {
                //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
            }

        } catch (Exception e) {
            e.printStackTrace();
            //BroadcastHelper.sendInform(BroadcastHelper.HUB_CONNECTION_ERROR_BROADCAST_METHOD, context);
        }
    }

    public static void ConfirmDelivery(final Context context, int orderID, boolean done, String reasons) {

        if (HubFactory.connection.getState() == ConnectionState.Connected && HubFactory.hub != null) {
            HubFactory.hub.invoke("ConfirmDelivery", orderID, done, reasons).done(new Action<Void>() {
                @Override
                public void run(Void aVoid) throws Exception {
                    Log.v(HUB_TAG, "ConfirmDelivery Ok");
                }
            }).onError(new ErrorCallback() {
                @Override
                public void onError(Throwable throwable) {
                    Log.v(HUB_TAG, "ConfirmDelivery ERROR");
                }
            });
        } else {
            BroadcastHelper.sendInform(BroadcastHelper.ERROR_POST_ORDER_SEND_BROADCAST_METHOD, context);
        }
    }   // ConfirmDelivery()

    public static void PostOrder(final Context context) {
        try {
            if (HubFactory.connection.getState() == ConnectionState.Connected && HubFactory.hub != null) {
                HubFactory.hub.invoke("ok").done(new Action<Void>() {
                    @Override
                    public void run(Void aVoid) throws Exception {
                        Log.v(HUB_TAG, "ok Ok");
                        BroadcastHelper.sendInform(BroadcastHelper.SUCCESS_POST_ORDER_SEND_BROADCAST_METHOD, context);
                    }
                }).onError(new ErrorCallback() {
                    @Override
                    public void onError(Throwable throwable) {
                        Log.v(HUB_TAG, "ok Error");
                        BroadcastHelper.sendInform(BroadcastHelper.ERROR_POST_ORDER_SEND_BROADCAST_METHOD, context);
                    }
                });

                HubFactory.hub.on("broadcastMessage", new SubscriptionHandler1<String>() {
                    @Override
                    public void run(String msg) {
                        Log.d("result := ", msg);
                    }
                }, String.class);
            } else {
                BroadcastHelper.sendInform(BroadcastHelper.ERROR_POST_ORDER_SEND_BROADCAST_METHOD, context);
            }
        } catch (Exception e) {
            e.printStackTrace();
            BroadcastHelper.sendInform(BroadcastHelper.ERROR_POST_ORDER_SEND_BROADCAST_METHOD, context);

        }
    }
}
