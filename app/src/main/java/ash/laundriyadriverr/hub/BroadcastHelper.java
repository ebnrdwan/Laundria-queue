package ash.laundriyadriverr.hub;


import android.content.Context;
import android.content.Intent;

public class BroadcastHelper {
    public static final String BROADCAST_ACTION_NAME = "ash.laundriyadriver";
    public static final String BROADCAST_EXTRA_METHOD_NAME = "method";
    public static final String UPDATE_CONNECTION_STATE_BROADCAST_METHOD = "updateConnection";
    public static final String HUB_CONNECTION_ERROR_BROADCAST_METHOD = "hubConnectionError";
    public static final String ERROR_POST_ORDER_SEND_BROADCAST_METHOD = "ErrorpostOrderSend";
    public static final String SUCCESS_POST_ORDER_SEND_BROADCAST_METHOD = "SuccesspostOrderSend";
    public static final String ORDER_REJECTED_BROADCAST_METHOD = "RejectedOrderWithRef";    //"regectedOrder";
    public static final String PENDING_ORDER_BROADCAST_METHOD = "PendingOrder"; //"pendingOrder";
    public static final String ORDER_CONFIRMED_BROADCAST_METHOD = "orderConfirmed";   //"confirmedOrder";
    public static final String ORDER_COOKED_BROADCAST_METHOD = "OrderCooked";  //"cookedOrder";
    public static final String ORDER_START_DEVLEVIRING_BROADCAST_METHOD = "OrderStartDelivering";
    public static final String DRIVER_UPDATE_BROADCAST_METHOD = "driverupdate";
    public static final String ORDER_DELIVERED_BROADCAST_METHOD = "OrderDeliveried"; // OrderDeliveried(orderId);
    public static final String DRIVER_CALLREPLY_BROADCAST_METHOD = "drivercallreply";


    public static void sendInform(String method , Context context)
    {
        Intent intent = new Intent();
        intent.setAction(BROADCAST_ACTION_NAME);
        intent.putExtra(BROADCAST_EXTRA_METHOD_NAME, method);
        try
        {
            if (context != null)
                context.sendBroadcast(intent);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }   // sendInform()
}
