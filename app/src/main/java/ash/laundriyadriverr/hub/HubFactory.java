package ash.laundriyadriverr.hub;

import android.content.Context;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;

import ash.laundriyadriverr.serverconnection.Url;
import ash.laundriyadriverr.utils.LoginSharedPreferences;
import microsoft.aspnet.signalr.client.ConnectionState;
import microsoft.aspnet.signalr.client.LogLevel;
import microsoft.aspnet.signalr.client.Logger;
import microsoft.aspnet.signalr.client.Platform;
import microsoft.aspnet.signalr.client.SignalRFuture;
import microsoft.aspnet.signalr.client.StateChangedCallback;
import microsoft.aspnet.signalr.client.http.android.AndroidPlatformComponent;
import microsoft.aspnet.signalr.client.hubs.HubConnection;
import microsoft.aspnet.signalr.client.hubs.HubProxy;
import microsoft.aspnet.signalr.client.transport.ClientTransport;
import microsoft.aspnet.signalr.client.transport.LongPollingTransport;
import microsoft.aspnet.signalr.client.transport.ServerSentEventsTransport;
import microsoft.aspnet.signalr.client.transport.WebsocketTransport;


public class HubFactory {

    public static HubConnection connection = null;
    public static HubProxy hub = null;
    public static ClientTransport transport = null;
    public static String accessToken = "";
    public static Context mContext = null;
    public static boolean forceStop = false;
    public static Timer mTimer = new Timer();
    private static HubFactory mHubFactory = null;
    public final String TAG = "HUB_FACTORY";
    public LoginSharedPreferences loginSharedPreferences;


    public HubFactory(Context Context, String aToken) {
        mContext = Context;
        accessToken = aToken;
    }

    public static HubFactory getInstance(Context context, String token) {
        mContext = context;
        accessToken = token;
        if (mHubFactory == null) {
            mHubFactory = new HubFactory(context, token);
        }
        return mHubFactory;
    }

    public HubProxy getTheHub() {
        try {
            loginSharedPreferences = new LoginSharedPreferences();
            if (hub != null) {
                return hub;
            }
            Log.v(TAG, "gettheHub");
            Platform.loadPlatformComponent(new AndroidPlatformComponent());
            connection = new HubConnection(Url.getInstance().HUB_HOST, "", true, new Logger() {
                @Override
                public void log(String s, LogLevel logLevel) {
                    Log.v(TAG, s);
                }
            });
            connection.getHeaders().put("Authorization", "Bearer " + accessToken);
            hub = connection.createHubProxy(Url.getInstance().HUB_NAME);
            transport = new WebsocketTransport(new Logger() {
                @Override
                public void log(String s, LogLevel logLevel) {
                    Log.v(TAG, s);
                }
            });
//            transport = new ServerSentEventsTransport(new Logger() {
//                @Override
//                public void log(String s, LogLevel logLevel) {
//                    Log.v(TAG, s);
//                }
//            });

            mTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    if (connection != null) {
                        if (connection.getState() == ConnectionState.Disconnected && !forceStop) {
                            try {
                                Log.v(TAG, "connection timer !!!!!!!!!!! retry");
                                // get
                                connection.getHeaders().put("Authorization", "Bearer " + loginSharedPreferences.getAccessToken(mContext));
                                connection.start(transport);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }, 5000, 5000);
            connection.stateChanged(new StateChangedCallback() {
                @Override
                public void stateChanged(ConnectionState connectionState, ConnectionState connectionState2) {
                    Log.v(TAG, "update connection title broadcast");
                    BroadcastHelper.sendInform(BroadcastHelper.UPDATE_CONNECTION_STATE_BROADCAST_METHOD, mContext);
                }
            });
            connection.closed(new Runnable() {
                @Override
                public void run() {

                    try {
                        Thread.sleep(5000);
                        Log.v(TAG, "connection closed !!!!!!!!!!! retry");
                        if (connection.getState() != ConnectionState.Connected && !forceStop) {
                            try {
                                connection.getHeaders().put("Authorization", "Bearer " + accessToken);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            SignalRFuture<Void> awaitConnection = connection.start(transport);
            try {
                awaitConnection.get();
            } catch (InterruptedException e) {
                Log.v(TAG, "Interrupted");
                e.printStackTrace();
            } catch (ExecutionException e) {
                Log.v(TAG, "Execution");
                e.printStackTrace();
            }
            hub.subscribe(AppStateData.getInstance(mContext));
            HubCalls.updateDriverLocation(mContext);
            return hub;
        } catch (Exception ex) {
            return getTheHub();
        }
    }   // getTheHub()
}