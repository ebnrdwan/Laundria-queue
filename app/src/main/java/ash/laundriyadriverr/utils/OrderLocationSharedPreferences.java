package ash.laundriyadriverr.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by shafeek on 04/08/16.
 */
public class OrderLocationSharedPreferences {

    private Context context;
    private String orderLat, orderLng, orderId, clientName, clientPhone, type, orderUpdated;
    SharedPreferences.Editor editor;
    SharedPreferences settings;

    public OrderLocationSharedPreferences() {
    }

    public OrderLocationSharedPreferences(Context context, String orderLat, String orderLng, String orderId, String clientName, String clientPhone, String type, String orderUpdated) {
        this.orderLat = orderLat;
        this.context = context;
        this.orderLng = orderLng;
        this.orderUpdated = orderUpdated;
        settings = context.getSharedPreferences("location", 0);
        editor = settings.edit();
        editor.putString("orderLat", orderLat);
        editor.putString("orderLng", orderLng);
        editor.putString("orderId", orderId);
        editor.putString("clientName", clientName);
        editor.putString("clientPhone", clientPhone);
        editor.putString("type", type);
        editor.putString("orderUpdated", orderUpdated);
        editor.commit();
    }

    public String getOrderUpdated(Context context) {
        settings = context.getSharedPreferences("location", 0);
        return settings.getString("orderUpdated", "0");
    }

    public void setOrderUpdated(Context context, String orderUpdated) {
        this.orderUpdated = orderUpdated;
        settings = context.getSharedPreferences("location", 0);
        editor = settings.edit();
        editor.putString("orderUpdated", orderUpdated);
        editor.commit();
    }

    public String getType(Context context) {
        settings = context.getSharedPreferences("location", 0);
        return settings.getString("type", "0");
    }

    public void setType(Context context, String type) {
        this.type = type;
        settings = context.getSharedPreferences("location", 0);
        editor = settings.edit();
        editor.putString("type", type);
        editor.commit();
    }


    public String getOrderId(Context context) {
        settings = context.getSharedPreferences("location", 0);
        return settings.getString("orderId", "0");
    }

    public void setOrderId(Context context, String orderId) {
        this.orderId = orderId;
        settings = context.getSharedPreferences("location", 0);
        editor = settings.edit();
        editor.putString("orderId", orderLng);
        editor.commit();
    }


    public String getClientName(Context context) {
        settings = context.getSharedPreferences("location", 0);
        return settings.getString("clientName", "0");
    }

    public void setClientName(Context context, String clientName) {
        this.clientName = clientName;
        settings = context.getSharedPreferences("location", 0);
        editor = settings.edit();
        editor.putString("clientName", orderLng);
        editor.commit();
    }

    public String getClientPhone(Context context) {
        settings = context.getSharedPreferences("location", 0);
        return settings.getString("clientPhone", "0");
    }

    public void setClientPhone(Context context, String clientPhone) {
        this.clientPhone = clientPhone;
        settings = context.getSharedPreferences("location", 0);
        editor = settings.edit();
        editor.putString("clientPhone", clientPhone);
        editor.commit();
    }


    public String getOrderLat(Context context) {
        settings = context.getSharedPreferences("location", 0);
        return settings.getString("orderLat", "0");
    }

    public void setOrderLat(Context context, String orderLat) {
        this.orderLat = orderLat;
        settings = context.getSharedPreferences("location", 0);
        editor = settings.edit();
        editor.putString("orderLat", orderLat);
        editor.commit();
    }

    public String getOrderLng(Context context) {
        settings = context.getSharedPreferences("location", 0);
        return settings.getString("orderLng", "0");
    }

    public void setOrderLng(Context context, String orderLng) {
        this.orderLng = orderLng;
        settings = context.getSharedPreferences("location", 0);
        editor = settings.edit();
        editor.putString("orderLng", orderLng);
        editor.commit();
    }



    public void removeLocation(Context context){
        settings = context.getSharedPreferences("location", 0);
        editor = settings.edit();
        editor.remove("orderLat");
        editor.remove("orderLng");
        editor.remove("orderId");
        editor.remove("clientName");
        editor.remove("clientPhone");
        editor.commit();
    }
}
