package ash.laundriyadriverr.utils;

import android.content.Context;
import android.content.SharedPreferences;


public class SharedPrefrenanceClass {
    public static String toStationKey = "TO_STATION";
    public static String toQueueKey = "TO_QUEUE";
    private static SharedPreferences pref;

    public static void saveData(Context con, String key, String value) {
        pref = con.getSharedPreferences("_Preferences", con.MODE_PRIVATE);

        SharedPreferences.Editor prefeditor = pref.edit();

        prefeditor.putString(key, value);

        prefeditor.commit();

    }

    public static void setToQueue(Context con, boolean tostation) {
        pref = con.getSharedPreferences("_Preferences", con.MODE_PRIVATE);

        SharedPreferences.Editor prefeditor = pref.edit();

        prefeditor.putBoolean(toQueueKey, tostation);
        prefeditor.commit();

    }

    public static boolean getToQueue(Context con, String key) {
        pref = con.getSharedPreferences("_Preferences", con.MODE_PRIVATE);

        return pref.getBoolean(key, false);
    }

    public static void savetoStation(Context con, boolean tostation) {
        pref = con.getSharedPreferences("_Preferences", con.MODE_PRIVATE);

        SharedPreferences.Editor prefeditor = pref.edit();

        prefeditor.putBoolean(toStationKey, tostation);
        prefeditor.commit();

    }

    public static boolean getToStation(Context con, String key) {
        pref = con.getSharedPreferences("_Preferences", con.MODE_PRIVATE);

        return pref.getBoolean(key, false);
    }
    public static String getValue(Context con, String key) {
        pref = con.getSharedPreferences("_Preferences", con.MODE_PRIVATE);

        return pref.getString(key, "");
    }


    public static void removeData(Context con, String key) {
        pref = con.getSharedPreferences("_Preferences", con.MODE_PRIVATE);

        SharedPreferences.Editor prefeditor = pref.edit();

        prefeditor.remove(key);

        prefeditor.commit();
    }

    public static void clearPrefs(Context con) {
        pref = con.getSharedPreferences("_Preferences", con.MODE_PRIVATE);

        SharedPreferences.Editor prefeditor = pref.edit();

        prefeditor.clear();

        prefeditor.commit();
    }
}
