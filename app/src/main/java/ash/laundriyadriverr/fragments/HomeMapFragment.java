package ash.laundriyadriverr.fragments;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import com.ahmadrosid.lib.drawroutemap.DrawMarker;
import com.ahmadrosid.lib.drawroutemap.DrawRouteMaps;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.neurenor.permissions.PermissionCallback;
import com.neurenor.permissions.PermissionsHelper;

import org.json.JSONObject;
import org.w3c.dom.Document;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import ash.laundriyadriverr.R;
import ash.laundriyadriverr.activities.MainActivity;
import ash.laundriyadriverr.activities.SplashScreenActivity;
import ash.laundriyadriverr.hub.HubCalls;
import ash.laundriyadriverr.serverconnection.Url;
import ash.laundriyadriverr.serverconnection.volley.AppController;
import ash.laundriyadriverr.serverconnection.volley.ConnectionVolley;
import ash.laundriyadriverr.utils.CheckResponse;
import ash.laundriyadriverr.utils.DataParser;
import ash.laundriyadriverr.utils.GMapV2Direction;
import ash.laundriyadriverr.utils.GPSTracker;
import ash.laundriyadriverr.utils.LoginSharedPreferences;
import ash.laundriyadriverr.utils.MapUtilies;
import ash.laundriyadriverr.utils.OrderLocationSharedPreferences;
import ash.laundriyadriverr.utils.SharedPrefrenanceClass;
import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import io.nlopez.smartlocation.location.config.LocationAccuracy;
import io.nlopez.smartlocation.location.config.LocationParams;


/**
 * Created by ahmed on 3/21/17.
 */

public class HomeMapFragment extends Fragment implements GoogleMap.OnMarkerClickListener,
        OnMapReadyCallback, Response.Listener, Response.ErrorListener {

    LatLng toStationLocation;
    View view;
    GoogleMap googleMap;
    double lat, lng, myLat, myLng;
    SupportMapFragment mMapFragment;
    FragmentTransaction fragmentTransaction;
    GPSTracker gpsTracker;
    PolylineOptions polylineOptions;
    LatLng src, des;
    OrderLocationSharedPreferences orderLocationSharedPreferences;
    Dialog dialog;
    GMapV2Direction gMapV2Direction;
    Document document;
    String requestedUrl;
    Button orderButton, toLandryButton;
    public static int ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE = 5469;
    Location mylocation;
    List<List<HashMap<String, String>>> routes;

    PermissionsHelper helper;
    public static boolean toStation = false;
    Marker marker;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        view = inflater.inflate(R.layout.home_map_fragment, container, false);
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.home_map_fragment, container, false);
        } catch (InflateException e) {
        /* map is already there, just return view as it is */
        }

        helper = new PermissionsHelper(getContext());
        initialization();
        checkDrawOverPermission();

        if (orderLocationSharedPreferences.getOrderLat(getActivity())!=null){
            if (orderLocationSharedPreferences.getOrderLat(getActivity()).equals("0")) {
                orderButton.setVisibility(View.GONE);
            }
        }


//        if (orderLocationSharedPreferences.getOrderUpdated(getActivity()).equals("0")) {
//            toLandryButton.setVisibility(View.GONE);
//        } else {
//            orderButton.setVisibility(View.GONE);
//        }
        startLocationListener();

        lat = Double.parseDouble(orderLocationSharedPreferences.getOrderLat(getActivity()));
        lng = Double.parseDouble(orderLocationSharedPreferences.getOrderLng(getActivity()));
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                HubCalls.clientConnected(getActivity());
//                HubCalls.orderReplay();
//                HubCalls.driverComing();
//                HubCalls.DriverHere();

//                if (gpsTracker.canGetLocation()) {
//                    ScheduledExecutorService scheduleTaskExecutor = Executors.newScheduledThreadPool(5);
//
//                    scheduleTaskExecutor.scheduleAtFixedRate(new Runnable() {
//                        public void run() {
//                            HubCalls.updateLocation(gpsTracker.getLatitude() + "", gpsTracker.getLongitude() + "");
//                        }
//                    }, 0, 2, TimeUnit.MINUTES);
//                } else {
//                    gpsTracker.showSettingsAlert();
//                }
            }
        }, 5000);

        orderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callServerToSyncData();
            }
        });

        toLandryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HubCalls.itemsToLaundry(orderLocationSharedPreferences.getOrderId(getActivity()), getActivity());
                Toast.makeText(getActivity(), getResources().getString(R.string.done), Toast.LENGTH_LONG).show();
                orderLocationSharedPreferences.removeLocation(getActivity());
                getActivity().finish();
                SharedPrefrenanceClass.setToQueue(getActivity(), true);
                Intent intent2 = new Intent(getActivity(), SplashScreenActivity.class);
//                        .putExtra("choose", "choose");
                startActivity(intent2);
                getActivity().finish();
            }
        });
        if (SharedPrefrenanceClass.getToQueue(getActivity(), SharedPrefrenanceClass.toQueueKey)) {
            showChooseDialog();
            SharedPrefrenanceClass.setToQueue(getActivity(), false);
        }


        return view;
    }


    public void showChooseDialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.choose_end_behavior);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Button proceedButton = (Button) dialog.findViewById(R.id.proceedButton);
        Button endButton = (Button) dialog.findViewById(R.id.endButton);
        Button freeME = (Button) dialog.findViewById(R.id.freeme);
        dialog.show();
        proceedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                HubCalls.moneyConfirm(orderLocationSharedPreferences.getOrderId(ConfirmatonActivity.this), ConfirmatonActivity.this);


                HubCalls.getItemsFromQueue(getActivity());
//                HubCalls.PostOrder(ConfirmatonActivity.this);
                Intent intent = new Intent(getActivity(), MainActivity.class).setAction("queue");
                startActivity(intent);
                // TODO: 19/11/2017 check to whether fetch new request or end day
//                EditOrderFragment fragment = new EditOrderFragment();
//                fragment.callServerToGetOrder();
                dialog.dismiss();

            }
        });
        endButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(getActivity(), MainActivity.class).setAction("station");
//                startActivity(intent);
//                SharedPrefrenanceClass.savetoStation(getActivity(), true);
                getStationLocation(getActivity());
                dialog.dismiss();
                Toast.makeText(getActivity(), "get location station", Toast.LENGTH_LONG).show();
            }
        });
        freeME.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                HubCalls.itemsToLaundry(orderLocationSharedPreferences.getOrderId(getActivity()), getActivity());
                Toast.makeText(getActivity(), "i'm Free", Toast.LENGTH_LONG).show();
                orderLocationSharedPreferences.removeLocation(getActivity());
                Intent intent2 = new Intent(getActivity(), SplashScreenActivity.class);
                startActivity(intent2);
                dialog.dismiss();
                getActivity().finish();
            }
        });
    }


    public void getStationLocation(final Context context) {
        final LoginSharedPreferences preferences = new LoginSharedPreferences();
        requestedUrl= "http://Service.laundriya.com/api/drivers/getLocationofDriverBranch";

        //try request1
        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestedUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getContext(), "error station location" + error.toString(), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                LoginSharedPreferences preferences = new LoginSharedPreferences();
                headers.put("Authorization", "bearer " + preferences.getAccessToken(context));
                return headers;

            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                LoginSharedPreferences preferences = new LoginSharedPreferences();
                params.put("mobilenumber", preferences.getMobile(context));
                return params;
            }
        };

        Map<String, String> parms = new HashMap<String, String>();
        parms.put("mobilenumber", preferences.getMobile(getActivity()));
        Log.e("Parm", parms.toString());
        AppController.getInstance().addToRequestQueue(stringRequest);

        //try request2
        ConnectionVolley connectionVolley = new ConnectionVolley(getActivity(), Request.Method.POST, requestedUrl, this, this, parms, true);
        AppController.getInstance().addToRequestQueue(connectionVolley, "");



        // try request3
        Map<String, String> postParam = new HashMap<String, String>();
        postParam.put("mobilenumber", preferences.getMobile(context));
        Log.d("stationPARAMS",postParam.toString());

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, requestedUrl, new JSONObject(postParam),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        progressDialog.dismiss();
//                        Log.d(TAG, response.toString());

                            String link = response.toString();
                            Toast.makeText(getActivity(),link,Toast.LENGTH_SHORT).show();

//                            startActivity(new Intent(getActivity(), PaymentActivity.class).putExtra("PURL",link));
//


                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
//                VolleyLog.d(TAG, "Error: " + error.getMessage());
//                progressDialog.dismiss();
            }
        }) {

            /**
             * Passing some request headers
             * */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "bearer " + preferences.getAccessToken(context));
                return headers;
            }


        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq, "TAG");


    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE) {
            if (!Settings.canDrawOverlays(getContext())) {
                // You don't have permission
                checkDrawOverPermission();

            } else {
                getLocationPermission();

            }

        }
    }

    private void checkDrawOverPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.canDrawOverlays(getActivity())) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + getActivity().getApplicationContext().getPackageName()));
                startActivityForResult(intent, ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE);
            } else {

                getLocationPermission();
            }
        } else {

            getLocationPermission();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        helper.onRequestPermissionsResult(permissions, grantResults);
    }

    private void getLocationPermission() {
        if (helper.isPermissionGranted(Manifest.permission.ACCESS_FINE_LOCATION)) {
            doOnLocationPermissionGranted();
        } else {
            helper.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, new PermissionCallback() {
                @Override
                public void onResponseReceived(final HashMap<String, PermissionsHelper.PermissionGrant> mapPermissionGrants) {
                    PermissionsHelper.PermissionGrant permissionGrant = mapPermissionGrants
                            .get(Manifest.permission.ACCESS_FINE_LOCATION);

                    switch (permissionGrant) {
                        case GRANTED:
                            //permission has been granted
                            // Toast.makeText(getContext(),"Granted",Toast.LENGTH_SHORT).show();
                            doOnLocationPermissionGranted();
                            break;
                        case DENIED:
                            //permission has been denied
                            Toast.makeText(getContext(), "Denied", Toast.LENGTH_SHORT).show();
                            break;
                        case NEVERSHOW:
                            //permission has been denied and never show has been selected. Open permission settings of the app.
                            Toast.makeText(getContext(), "Denied with Never show", Toast.LENGTH_SHORT).show();
                            break;
                    }
                }
            });
        }
    }

    void doOnLocationPermissionGranted() {
        gpsTracker = new GPSTracker(getActivity());
        myLat = gpsTracker.getLatitude();
        myLng = gpsTracker.getLongitude();
        if (gpsTracker.canGetLocation()) {
            ScheduledExecutorService scheduleTaskExecutor = Executors.newScheduledThreadPool(5);

            scheduleTaskExecutor.scheduleAtFixedRate(new Runnable() {
                public void run() {
                    HubCalls.updateLocation(gpsTracker.getLatitude() + "", gpsTracker.getLongitude() + "");
                }
            }, 0, 2, TimeUnit.MINUTES);
        } else {
            gpsTracker.showSettingsAlert();
        }
    }

    private void initialization() {
        if (view!=null){
            orderButton = (Button) view.findViewById(R.id.orderButton);
            mMapFragment = SupportMapFragment.newInstance();
            fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.care_mapFrag, mMapFragment);
            fragmentTransaction.commit();
            mMapFragment.getMapAsync(this);
            orderLocationSharedPreferences = new OrderLocationSharedPreferences();
            toLandryButton = (Button) view.findViewById(R.id.toLandryButton);
        }
        else {
            getActivity().startActivity(new Intent(getActivity(),MainActivity.class));
        }

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return true;
    }

    private void flyTo(GoogleMap map, CameraPosition position, int milliSeconds) {
        map.animateCamera(CameraUpdateFactory.newCameraPosition(position), milliSeconds, null);
    }


    private void startLocationListener() {

        long mLocTrackingInterval = 1000 * 2; // 2 sec
        float trackingDistance = 0;
        LocationAccuracy trackingAccuracy = LocationAccuracy.HIGH;

        LocationParams.Builder builder = new LocationParams.Builder()
                .setAccuracy(trackingAccuracy)
                .setDistance(trackingDistance)
                .setInterval(mLocTrackingInterval);


        SmartLocation.with(getActivity())
                .location()
                .continuous()
                .config(builder.build())
                .start(new OnLocationUpdatedListener() {
                    @Override
                    public void onLocationUpdated(Location location) {
                        mylocation = location;
                        myLat = location.getLatitude();
                        myLng = location.getLongitude();
                        LatLng currentlocation = new LatLng(location.getLatitude(), location.getLongitude());
                        CameraPosition position = new CameraPosition.Builder().target(currentlocation).zoom(15).build();
                        if (googleMap != null)
                            flyTo(googleMap, position, 1000);
//                        googleMap.clear();
                        MarkerOptions marker = new MarkerOptions().position(currentlocation).title("ME");

//                        googleMap.addMarker(marker).setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
//                        googleMap.addMarker(new MarkerOptions().position(currentlocation).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_loaction)));

//                        Toast.makeText(getActivity(), "new Location:" + location.getLatitude() + "::" + location.getLongitude(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        googleMap.clear();
        this.googleMap = googleMap;
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        startLocationListener();
        googleMap.setMyLocationEnabled(true);

        des = new LatLng(30.1587831890036, 31.1587831890036);
        LatLng c = new LatLng(myLat, myLng);
//        drawMyDream(googleMap, c, des);
//        drawDirections(googleMap, c, des);
        LatLng latLngSrc = new LatLng(myLat, myLng);
        LatLng latLngDes = new LatLng(lat, lng);
//        drawMyDream(googleMap, latLngSrc, latLngDes);
        drawDirections(googleMap, latLngSrc, latLngDes);

        try {

//            drawMyDream(googleMap, c, latLngDes);


//            new GetDirection().execute();

            des = new LatLng(30.1587831890036, 31.1587831890036);


            if (toStation) {
//
                des = new LatLng(24.1587831890036, 24.1587831890036);
                marker = googleMap.addMarker(new MarkerOptions().position(des).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));

            } else {

                if (marker != null)
                    marker.remove();
                des = latLngDes;
            }
//            drawMyDream(googleMap, src, des);


            src = latLngSrc;
            Log.d("STATION", String.valueOf(toStation));


            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                return;
            } else {
                googleMap.setMyLocationEnabled(true);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // second method to get directions (Library way)
    public void drawMyDream(GoogleMap mMap, LatLng origin, LatLng destination) {

        googleMap.clear();
        destination = des = new LatLng(30.1587831890036, 31.1587831890036);
        DrawRouteMaps.getInstance(getActivity())
                .draw(origin, destination, mMap);


        DrawMarker.getInstance(getActivity()).draw(mMap, origin, R.drawable.ic_loaction, "Origin Location");
        DrawMarker.getInstance(getActivity()).draw(mMap, destination, R.drawable.map_des, "Destination Location");

        LatLngBounds bounds = new LatLngBounds.Builder()
                .include(origin)
                .include(destination).build();
        Point displaySize = new Point();
        getActivity().getWindowManager().getDefaultDisplay().getSize(displaySize);
        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, displaySize.x, 250, 30));

    }


    // first method to draw directions (static way)
    private void drawDirections(final GoogleMap map, LatLng srs, LatLng dest) {
        String url = MapUtilies.getUrl(srs, dest);
        RequestQueue requestDirections = Volley.newRequestQueue(getActivity());
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                DataParser parser = new DataParser();
                routes = parser.parse(response);
                ArrayList<LatLng> points;
                PolylineOptions lineOptions = null;

                // Traversing through all the routes
                for (int i = 0; i < routes.size(); i++) {
                    points = new ArrayList<>();
                    lineOptions = new PolylineOptions();

                    // Fetching i-th route
                    List<HashMap<String, String>> path = routes.get(i);
                    Log.d("DRAW_Path", path.toString());

                    // Fetching all the points in i-th route
                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);

                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);

                        points.add(position);
                        Log.d("DRAW_POINTS", String.valueOf(point.size()));

                    }

                    // Adding all the points in the route to LineOptions
                    lineOptions.addAll(points);
                    lineOptions.width(7);
                    lineOptions.color(Color.parseColor("#FF4081"));
                    map.addPolyline(lineOptions);


                    Log.d("drawDirection", "onPostExecute lineoptions decoded:" + lineOptions.toString());

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("errorRout", error.toString());
            }
        });

        requestDirections.add(jsonRequest);

    }

    public class GetDirection extends AsyncTask<String, Void, String> {


        @Override
        protected String doInBackground(String... params) {
            gMapV2Direction = new GMapV2Direction();

            document = gMapV2Direction.getDocument(src, des,
                    GMapV2Direction.MODE_DRIVING);
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
            dialog.dismiss();
            try {
                Log.d("STATION", String.valueOf(toStation));

//                calculateTime(gMapV2Direction.getTimeValue(document));
//                calculateDistance(gMapV2Direction.getDistanceValue(document));
                ArrayList<LatLng> directionPoint = gMapV2Direction.getDirection(document);
                polylineOptions = new PolylineOptions().width(5).color(Color.RED);
                for (int i = 0; i < directionPoint.size(); i++)
                    polylineOptions.add(directionPoint.get(i));
                googleMap.addPolyline(polylineOptions);
                DOMSource domSource = new DOMSource(document);
                StringWriter writer = new StringWriter();
                StreamResult resultt = new StreamResult(writer);
                TransformerFactory tf = TransformerFactory.newInstance();
                Transformer transformer = tf.newTransformer();
                transformer.transform(domSource, resultt);
                toStation = false;
                dialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onPreExecute() {
            dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.progress_dialog);
            dialog.setCancelable(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
        }
    }

    private void callServerToSyncData() {
        requestedUrl = Url.getInstance().syncDataURL;
        Map<String, String> parms = new HashMap<String, String>();
        parms.put("SyncDate", "2/2/1999");
        Log.e("Parm", parms.toString());
        ConnectionVolley connectionVolley = new ConnectionVolley(getActivity(), Request.Method.POST, Url.getInstance().syncDataURL, this, this, parms, true);
        AppController.getInstance().addToRequestQueue(connectionVolley, "");
    }

    private void toEditOrderFragment(String getJson) {
        EditOrderFragment editOrderFragment = new EditOrderFragment();
        Bundle args = new Bundle();
        args.putString("json", getJson);
        editOrderFragment.setArguments(args);
        getActivity().getSupportFragmentManager().beginTransaction().add(R.id.contentView, editOrderFragment).addToBackStack("").commit();
    }

    @Override
    public void onResponse(Object response) {
        Log.e("response", response.toString());
        try {
            if (requestedUrl.equals(Url.getInstance().syncDataURL)) {
                if (CheckResponse.getInstance().checkResponse(getActivity(), response.toString(), true)) {
                    toEditOrderFragment(response.toString());
                }
            }
            else if (requestedUrl.equals("http://Service.laundriya.com/api/drivers/getLocationofDriverBranch")){


                String[] responsee = response.toString().split(",");
                Toast.makeText(getActivity(), "your location:" + response.toString(), Toast.LENGTH_SHORT).show();

                String thlat = responsee[0]; //sub first
                String thLng = responsee[1]; //sub last
                thlat = thlat.substring(1,thlat.length());
                thLng = thLng.substring(0,thLng.length()-1);
                double laat = Double.parseDouble(thlat);
                double laang = Double.parseDouble(thLng);
                toStationLocation = new LatLng(laat, laang);
                src = new LatLng(mylocation.getLatitude(),mylocation.getLongitude());
                googleMap.clear();
                drawDirections(googleMap, src, toStationLocation);

            }
        } catch (Exception e) {
            Toast.makeText(getActivity(), getResources().getString(R.string.connectionError), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        VolleyLog.d("responseError", "Error: " + error.getMessage());
        try {
            ConnectionVolley.dialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
