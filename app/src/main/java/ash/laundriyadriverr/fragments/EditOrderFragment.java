package ash.laundriyadriverr.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ash.laundriyadriverr.R;
import ash.laundriyadriverr.activities.SplashScreenActivity;
import ash.laundriyadriverr.hub.HubCalls;
import ash.laundriyadriverr.models.CategoryModel;
import ash.laundriyadriverr.models.ItemModel;
import ash.laundriyadriverr.models.OrderItemsModel;
import ash.laundriyadriverr.models.OrderModel;
import ash.laundriyadriverr.models.PriceModel;
import ash.laundriyadriverr.models.ServiceModel;
import ash.laundriyadriverr.models.ServiceTypeModel;
import ash.laundriyadriverr.models.StandardWebServiceResponse;
import ash.laundriyadriverr.models.SyncDataModel;
import ash.laundriyadriverr.serverconnection.Url;
import ash.laundriyadriverr.serverconnection.volley.AppController;
import ash.laundriyadriverr.serverconnection.volley.ConnectionVolley;
import ash.laundriyadriverr.utils.CheckResponse;
import ash.laundriyadriverr.utils.GeneralClass;
import ash.laundriyadriverr.utils.OrderLocationSharedPreferences;

/**
 * Created by ahmed on 2/7/17.
 */

public class EditOrderFragment extends Fragment implements View.OnClickListener, Response.Listener, Response.ErrorListener {

    View view;
    ListView itemsListView;
    ImageView addItemButton;
    ArrayList<OrderItemsModel> orderItemsModelArrayList;
    ArrayList<CategoryModel> categoryModelArrayList;
    ArrayList<ItemModel> itemModelArrayList;
    ArrayList<ServiceModel> serviceModelArrayList;
    ArrayList<ServiceTypeModel> serviceTypeModels;
    ArrayList<PriceModel> priceModelArrayList;
    String getJson;
    ItemsAdapter itemsAdapter;
    String requestedUrl;
    Button updateButton, iamComingButton, iamHereButton, toLandryButton;
    TextView clientNameTextView, clientPhoneTextView;
    OrderLocationSharedPreferences orderLocationSharedPreferences;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.edit_order_fragment, container, false);
        initilization();
        decodeJson(getJson);
        set();
        callAdapter();

        if (orderLocationSharedPreferences.getType(getActivity()).equals("1")){
            iamComingButton.setVisibility(View.GONE);
        } else {
            iamComingButton.setVisibility(View.VISIBLE);
            updateButton.setVisibility(View.GONE);
            addItemButton.setVisibility(View.GONE);
            Toast.makeText(getActivity(), "dfj", Toast.LENGTH_SHORT).show();
            callServerToGetOrder();
        }

        itemsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {
                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.delete_dialog);
                dialog.setCancelable(true);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Button yesButton = (Button) dialog.findViewById(R.id.yesButton);
                yesButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        orderItemsModelArrayList.remove(i);
                        callAdapter();
                    }
                });
                Button noButton = (Button) dialog.findViewById(R.id.noButton);
                noButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
        return view;
    }

    private void initilization(){
        itemsListView = (ListView) view.findViewById(R.id.itemsListView);
        addItemButton = (ImageView) view.findViewById(R.id.addItemButton);
        updateButton = (Button) view.findViewById(R.id.updateButton);
        iamComingButton = (Button) view.findViewById(R.id.iamComingButton);
        iamHereButton = (Button) view.findViewById(R.id.iamHereButton);
        toLandryButton = (Button) view.findViewById(R.id.toLandryButton);
        clientNameTextView = (TextView) view.findViewById(R.id.clientNameTextView);
        clientPhoneTextView = (TextView) view.findViewById(R.id.clientPhoneTextView);
        orderItemsModelArrayList = new ArrayList<>();
        categoryModelArrayList = new ArrayList<>();
        itemModelArrayList = new ArrayList<>();
        serviceModelArrayList = new ArrayList<>();
        serviceTypeModels = new ArrayList<>();
        priceModelArrayList = new ArrayList<>();
        getJson = getArguments().getString("json");
        requestedUrl = "";
        orderLocationSharedPreferences = new OrderLocationSharedPreferences();
    }

    private void set(){
        addItemButton.setOnClickListener(this);
        updateButton.setOnClickListener(this);
        iamComingButton.setOnClickListener(this);
        iamHereButton.setOnClickListener(this);
        toLandryButton.setOnClickListener(this);
        clientNameTextView.setText(orderLocationSharedPreferences.getClientName(getActivity()));
        clientPhoneTextView.setText(orderLocationSharedPreferences.getClientPhone(getActivity()));
    }

    // TODO: 19/11/2017 fetching order from server
    public void callServerToGetOrder(){
//        requestedUrl = Url.getInstance().getOrderURL;
        Map<String,String> parms = new HashMap<String,String>();
        parms.put("OrderId", orderLocationSharedPreferences.getOrderId(getActivity()));
        Log.e("Parm", parms.toString());
        ConnectionVolley connectionVolley = new ConnectionVolley(getActivity(), Request.Method.POST, Url.getInstance().getOrderURL, this, this, parms, true);
        AppController.getInstance().addToRequestQueue(connectionVolley, "");
    }

    private void decodeJson(String jsonString){
        try {
            Gson gson = new Gson();
            SyncDataModel syncDataModel = new SyncDataModel();
            gson = new Gson();
            syncDataModel = gson.fromJson(jsonString, SyncDataModel.class);
            categoryModelArrayList = new ArrayList<>();
            gson = new Gson();
            GsonBuilder builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();
            categoryModelArrayList = gson.fromJson(builder.create().toJson(syncDataModel.getCategories()), new TypeToken<List<CategoryModel>>(){}.getType());

            itemModelArrayList = new ArrayList<>();
            gson = new Gson();
            builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();
            itemModelArrayList = gson.fromJson(builder.create().toJson(syncDataModel.getItems()), new TypeToken<List<ItemModel>>(){}.getType());

            serviceModelArrayList = new ArrayList<>();
            gson = new Gson();
            builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();
            serviceModelArrayList = gson.fromJson(builder.create().toJson(syncDataModel.getServices()), new TypeToken<List<ServiceModel>>(){}.getType());

            serviceTypeModels = new ArrayList<>();
            gson = new Gson();
            builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();
            serviceTypeModels = gson.fromJson(builder.create().toJson(syncDataModel.getServicestypes()), new TypeToken<List<ServiceTypeModel>>(){}.getType());

            priceModelArrayList = new ArrayList<>();
            gson = new Gson();
            builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();
            priceModelArrayList = gson.fromJson(builder.create().toJson(syncDataModel.getPrices()), new TypeToken<List<PriceModel>>(){}.getType());

//            orderItemsModelArrayList = new ArrayList<>();
//            gson = new Gson();
//            builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();
//            orderItemsModelArrayL ist = gson.fromJson(orderModel.getOrderItems().toString(), new TypeToken<List<OrderItemsModel>>(){}.getType());

            if (orderItemsModelArrayList.size() != 0){
                addItemButton.setVisibility(View.GONE);
                updateButton.setVisibility(View.GONE);
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private void decodeJson2(String jsonString){
        try {
            Gson gson = new Gson();
            StandardWebServiceResponse standardWebServiceResponse = new StandardWebServiceResponse();
            standardWebServiceResponse = gson.fromJson(jsonString, StandardWebServiceResponse.class);
            OrderModel orderModel = new OrderModel();
            gson = new Gson();
            GsonBuilder builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();
            orderModel = gson.fromJson(builder.create().toJson(standardWebServiceResponse.getData()), OrderModel.class);
            Toast.makeText(getActivity(), standardWebServiceResponse.getData()+"", Toast.LENGTH_LONG).show();
            orderItemsModelArrayList = new ArrayList<>();
            gson = new Gson();
            builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();
            orderItemsModelArrayList = gson.fromJson(builder.create().toJson(orderModel.getOrderitems()), new TypeToken<List<OrderItemsModel>>(){}.getType());

            Toast.makeText(getActivity(), orderModel.getOrderItems()+"", Toast.LENGTH_LONG).show();
            callAdapter();
//            if (orderItemsModelArrayList.size() != 0){
//                addItemButton.setVisibility(View.GONE);
//                updateButton.setVisibility(View.GONE);
//            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.addItemButton:
                addDialog();
                break;
            case R.id.updateButton:
                if (orderItemsModelArrayList.size() != 0){
                    callServerToUpdateOrder();
                    orderLocationSharedPreferences.setOrderUpdated(getActivity(), "1");
                }
                break;
            case R.id.iamHereButton:
                HubCalls.DriverHere(orderLocationSharedPreferences.getOrderId(getActivity()));
                Toast.makeText(getActivity(), getResources().getString(R.string.done), Toast.LENGTH_LONG).show();
                break;
            case R.id.iamComingButton:
                HubCalls.driverComing(orderLocationSharedPreferences.getOrderId(getActivity()), getActivity());
                Toast.makeText(getActivity(), getResources().getString(R.string.done), Toast.LENGTH_LONG).show();
                break;
            case R.id.toLandryButton:
                HubCalls.itemsToLaundry(orderLocationSharedPreferences.getOrderId(getActivity()), getActivity());
                Toast.makeText(getActivity(), getResources().getString(R.string.done), Toast.LENGTH_LONG).show();
                orderLocationSharedPreferences.removeLocation(getActivity());
                getActivity().getSupportFragmentManager().popBackStack();
                getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
                getActivity().finish();
                Intent intent2 = new Intent(getActivity(), SplashScreenActivity.class);
                startActivity(intent2);
                break;
        }
    }

    private void callServerToUpdateOrder(){
        requestedUrl = Url.getInstance().updateOrderURL;

        Gson gson = new Gson();
        JsonElement element = gson.toJsonTree(orderItemsModelArrayList, new TypeToken<List<OrderItemsModel>>() {}.getType());

        Map<String,String> parms = new HashMap<String,String>();
        for (int i = 0; i < orderItemsModelArrayList.size(); i++){
            parms.put("ItemsList["+i+"][id]", orderItemsModelArrayList.get(i).getId());
            parms.put("ItemsList["+i+"][Amount]", orderItemsModelArrayList.get(i).getAmount());
            parms.put("ItemsList["+i+"][Notes]", orderItemsModelArrayList.get(i).getNotes());
            try {
                parms.put("ItemsList["+i+"][serviceId]", orderItemsModelArrayList.get(i).getServiceId());
                parms.put("ItemsList["+i+"][TypeId]", orderItemsModelArrayList.get(i).getTypeId());

            } catch (Exception e){
                parms.put("ItemsList["+i+"][serviceId]", "1");
                parms.put("ItemsList["+i+"][TypeId]", "1");
                e.printStackTrace();
            }
        }
        HubCalls.updateOrder(getActivity(), orderLocationSharedPreferences.getOrderId(getActivity()));
//        parms.put("ItemsList", element.getAsJsonArray().toString());
        parms.put("OrderId", orderLocationSharedPreferences.getOrderId(getActivity()));
        Log.e("Parm", parms.toString());
        ConnectionVolley connectionVolley = new ConnectionVolley(getActivity(), Request.Method.POST, Url.getInstance().updateOrderURL, this, this, parms, true);
        AppController.getInstance().addToRequestQueue(connectionVolley, "");
    }

    private void addDialog(){
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.items_dialog);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Button addButton = (Button)  dialog.findViewById(R.id.addButton);
        final Spinner itemSpinner = (Spinner)  dialog.findViewById(R.id.itemSpinner);
        final Spinner serviceSpinner = (Spinner)  dialog.findViewById(R.id.serviceSpinner);
        final Spinner serviceTypeSpinner = (Spinner)  dialog.findViewById(R.id.serviceTypeSpinner);
        final Spinner categorySpinner = (Spinner) dialog.findViewById(R.id.categorySpinner);
        TextView plusButton = (TextView) dialog.findViewById(R.id.plusButton);
        TextView minusButton = (TextView) dialog.findViewById(R.id.minusButton);
        final EditText counterEditText = (EditText) dialog.findViewById(R.id.counterEditText);

        ArrayAdapter<CategoryModel> spinnerAdapter4 = new ArrayAdapter<CategoryModel>(getActivity(), android.R.layout.simple_spinner_dropdown_item, categoryModelArrayList);
        spinnerAdapter4.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        categorySpinner.setAdapter(spinnerAdapter4);

        ArrayAdapter<ItemModel> spinnerAdapter = new ArrayAdapter<ItemModel>(getActivity(), android.R.layout.simple_spinner_dropdown_item, itemModelArrayList);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        itemSpinner.setAdapter(spinnerAdapter);

        ArrayAdapter<ServiceModel> spinnerAdapter2 = new ArrayAdapter<ServiceModel>(getActivity(), android.R.layout.simple_spinner_dropdown_item, serviceModelArrayList);
        spinnerAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        serviceSpinner.setAdapter(spinnerAdapter2);

        ArrayAdapter<ServiceTypeModel> spinnerAdapter3 = new ArrayAdapter<ServiceTypeModel>(getActivity(), android.R.layout.simple_spinner_dropdown_item, serviceTypeModels);
        spinnerAdapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        serviceTypeSpinner.setAdapter(spinnerAdapter3);

        plusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                counterEditText.setText(Integer.parseInt(counterEditText.getText().toString()) + 1 + "");
            }
        });

        minusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!counterEditText.getText().toString().equals("1")){
                    counterEditText.setText(Integer.parseInt(counterEditText.getText().toString()) - 1 + "");
                }
            }
        });

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                int count = priceModelArrayList.size();
                for (int i = 0; i < count; i++){
                    if (
                            priceModelArrayList.get(i).getItem().equals(itemModelArrayList.get(itemSpinner.getSelectedItemPosition()).getId())
                            && priceModelArrayList.get(i).getServiceId().equals(serviceModelArrayList.get(serviceSpinner.getSelectedItemPosition()).getId())
                            && priceModelArrayList.get(i).getServiceTypeId().equals(serviceTypeModels.get(serviceTypeSpinner.getSelectedItemPosition()).getId())
                            ){
                        double total = Double.parseDouble(priceModelArrayList.get(i).getPrice()) * Double.parseDouble(counterEditText.getText().toString());
                        orderItemsModelArrayList.add(new OrderItemsModel(itemModelArrayList.get(itemSpinner.getSelectedItemPosition()).getId(),
                                counterEditText.getText().toString(), total + "", serviceModelArrayList.get(serviceSpinner.getSelectedItemPosition()).getId(), serviceTypeModels.get(serviceTypeSpinner.getSelectedItemPosition()).getId(),
                                itemModelArrayList.get(itemSpinner.getSelectedItemPosition()).getArabicName(),
                                itemModelArrayList.get(itemSpinner.getSelectedItemPosition()).getEnglishName()));

//                        if(itemModelArrayList.contains(itemModelArrayList.get(i).getId()) && itemModelArrayList.contains(priceModelArrayList.get(i).getServiceId())&& itemModelArrayList.contains(priceModelArrayList.get(i).getServiceTypeId() )){
//                          continue;mail=> wasltec2@gmail.com
//                        password=> W9DA2c#0#74987
//                        }
                    }
                }
                itemsAdapter.notifyDataSetChanged();
                if (count != priceModelArrayList.size()){
                    Toast.makeText(getActivity(), getResources().getString(R.string.priceNotAvailableNow), Toast.LENGTH_SHORT).show();
                }
            }
        });
        dialog.show();
    }

    private void callAdapter(){

        itemsAdapter = new ItemsAdapter(getActivity(), orderItemsModelArrayList);

        itemsListView.setAdapter(itemsAdapter);
    }

    public class ItemsAdapter extends BaseAdapter {

        Activity activity;
        ArrayList<OrderItemsModel> orderModelArrayList;
        ViewHolder holder;

        public ItemsAdapter(){
        }

        public ItemsAdapter(Activity activity, ArrayList<OrderItemsModel> orderModelArrayList) {
            this.activity = activity;
            this.orderModelArrayList = orderModelArrayList;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            convertView = activity.getLayoutInflater().inflate(R.layout.edit_item_custom, parent, false);
            holder = new ViewHolder();
//            holder.orderImageView = (ImageView) convertView.findViewById(R.id.orderImageView);
            holder.itemNameTextView = (TextView) convertView.findViewById(R.id.itemNameTextView);
            holder.amountTextView = (TextView) convertView.findViewById(R.id.amountTextView);
            holder.priceTextView = (TextView) convertView.findViewById(R.id.priceTextView);

            if (GeneralClass.language.equals("en"))
                holder.itemNameTextView.setText(orderModelArrayList.get(position).getItemEnglishName());
            else
                holder.itemNameTextView.setText(orderModelArrayList.get(position).getItemArabicName());
            holder.amountTextView.setText(orderModelArrayList.get(position).getAmount());
            holder.priceTextView.setText(orderModelArrayList.get(position).getNotes());
//            Picasso.with(activity)
//                    .load(Url.getInstance().profilePicturesURL + serviceListModels.get(position).getPicturePath())
//                    .placeholder(R.mipmap.logo)
//                    .into(holder.orderImageView);
//            if (((Double)Double.parseDouble(serviceListModels.get(position).getDistance())).intValue() > 1000)
//                holder.distanceTextView.setText(activity.getResources().getString(R.string.distance) + " " +  orderModelArrayList.get(position).getDistance() + activity.getResources().getString(R.string.km));
//            else if (Integer.parseInt(serviceListModels.get(position).getDistance()) < 1000)

            return convertView;
        }

        public int getCount() {
            return orderModelArrayList.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        private class ViewHolder {
            private TextView itemNameTextView;
            private TextView amountTextView;
            private TextView priceTextView;
        }
    }

    @Override
    public void onResponse(Object response) {
        Log.e("response", response.toString());
        try {
            if (requestedUrl.equals(Url.getInstance().updateOrderURL)){
                if (CheckResponse.getInstance().checkResponse(getActivity(), response.toString(), true)) {
                    //toEditOrderFragment(response.toString());
                    getActivity().finish();
                    Intent intent = new Intent(getActivity(), SplashScreenActivity.class);
                    startActivity(intent);
                }
            } else if (requestedUrl.equals(Url.getInstance().getOrderURL)){
                if (CheckResponse.getInstance().checkResponse(getActivity(), response.toString(), true)) {
                    decodeJson2(response.toString());
                }
            }
        } catch (Exception e){
            Toast.makeText(getActivity(), getResources().getString(R.string.connectionError), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        VolleyLog.d("responseError", "Error: " + error.getMessage());
        try {
            ConnectionVolley.dialog.dismiss();
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}