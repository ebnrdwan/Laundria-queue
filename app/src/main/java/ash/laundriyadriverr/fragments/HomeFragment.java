package ash.laundriyadriverr.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import ash.laundriyadriverr.R;
import ash.laundriyadriverr.hub.HubCalls;
import ash.laundriyadriverr.models.OrderModel;
import ash.laundriyadriverr.models.StandardWebServiceResponse;
import ash.laundriyadriverr.serverconnection.Url;
import ash.laundriyadriverr.serverconnection.volley.AppController;
import ash.laundriyadriverr.serverconnection.volley.ConnectionVolley;
import ash.laundriyadriverr.utils.CheckResponse;
import ash.laundriyadriverr.utils.GPSTracker;

/**
 * Created by ahmed on 2/6/17.
 */

public class HomeFragment extends Fragment implements AdapterView.OnItemClickListener, SwipeRefreshLayout.OnRefreshListener , Response.Listener, Response.ErrorListener {

    View view;
    ListView ordersListView;
    String getJson;
    OrderAdapter orderAdapter;
    ArrayList<OrderModel> orderModelArrayList;
    String requestedUrl;
    GPSTracker tracker;
    SwipeRefreshLayout swipeRefreshListView;
    int index;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.home_fragment, container, false);
        initilization();
        set();
        decodeJson(getJson);
        callAdapter();
        callServerToGetOrderList();

       // HubCalls.PostOrder(getActivity());
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                HubCalls.clientConnected(getActivity());
//                HubCalls.orderReplay();
//                HubCalls.driverComing();
//                HubCalls.DriverHere();

                if(tracker.canGetLocation()){
                    ScheduledExecutorService scheduleTaskExecutor = Executors.newScheduledThreadPool(5);

                    scheduleTaskExecutor.scheduleAtFixedRate(new Runnable() {
                        public void run() {
                            HubCalls.updateLocation(tracker.getLatitude()+"", tracker.getLongitude()+"");
                        }
                    }, 0, 2, TimeUnit.MINUTES);
                }else {
                    tracker.showSettingsAlert();
                }


            }
        }, 5000);

        return view;
    }

    private void initilization(){
        ordersListView = (ListView) view.findViewById(R.id.ordersListView);
        swipeRefreshListView = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshListView);
        orderModelArrayList = new ArrayList<>();
        getJson = getArguments().getString("json");
        tracker = new GPSTracker(getActivity());
        swipeRefreshListView.setOnRefreshListener(this);
    }

    @Override
    public void onRefresh() {
        callServerToGetOrderList();
    }

    private void decodeJson(String jsonString){
        try {
            StandardWebServiceResponse standardWebServiceResponse = new StandardWebServiceResponse();
            Gson gson = new Gson();
            standardWebServiceResponse = gson.fromJson(jsonString, StandardWebServiceResponse.class);
            orderModelArrayList = new ArrayList<>();
            gson = new Gson();
            GsonBuilder builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();
            orderModelArrayList = gson.fromJson(builder.create().toJson(standardWebServiceResponse.getData()), new TypeToken<List<OrderModel>>(){}.getType());
            callAdapter();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private void callServerToGetOrderList(){
        requestedUrl = Url.getInstance().orderListURL;
        Map<String,String> parms = new HashMap<String,String>();
        parms.put("lat", "30.1");
        parms.put("lng", "31.2");
        Log.e("Parm", parms.toString());
        ConnectionVolley connectionVolley = new ConnectionVolley(getActivity(), Request.Method.POST, Url.getInstance().orderListURL, this, this, parms, true);
        AppController.getInstance().addToRequestQueue(connectionVolley, "");
    }

    private void set(){
        ordersListView.setOnItemClickListener(this);
    }

    private void callAdapter(){
        orderAdapter = new OrderAdapter(getActivity(), orderModelArrayList);
        ordersListView.setAdapter(orderAdapter);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        index = i;
        callServerToSyncData();
    }

    private void callServerToSyncData(){
        requestedUrl = Url.getInstance().syncDataURL;
        Map<String,String> parms = new HashMap<String,String>();
        parms.put("SyncDate", "2/2/1999");
        Log.e("Parm", parms.toString());
        ConnectionVolley connectionVolley = new ConnectionVolley(getActivity(), Request.Method.POST, Url.getInstance().syncDataURL, this, this, parms, true);
        AppController.getInstance().addToRequestQueue(connectionVolley, "");
    }

    public class OrderAdapter extends BaseAdapter {

        Activity activity;
        ArrayList<OrderModel> orderModelArrayList;
        ViewHolder holder;

        public OrderAdapter(){
        }

        public OrderAdapter(Activity activity, ArrayList<OrderModel> orderModelArrayList) {
            this.activity = activity;
            this.orderModelArrayList = orderModelArrayList;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            convertView = activity.getLayoutInflater().inflate(R.layout.order_custom, parent, false);
            holder = new ViewHolder();
            holder.orderImageView = (ImageView) convertView.findViewById(R.id.orderImageView);
            holder.orderNameTextView = (TextView) convertView.findViewById(R.id.orderNameTextView);
            holder.distanceTextView = (TextView) convertView.findViewById(R.id.distanceTextView);
            holder.locationImageView = (ImageView) convertView.findViewById(R.id.locationImageView);

            holder.locationImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(tracker.canGetLocation()){
                        TrackingFragment trackingFragment = new TrackingFragment();
                        Bundle args = new Bundle();
                        args.putString("lat", orderModelArrayList.get(position).getLatitude());
                        args.putString("lng", orderModelArrayList.get(position).getLogitude());
                        trackingFragment.setArguments(args);
                        getActivity().getSupportFragmentManager().beginTransaction().addToBackStack("").add(R.id.contentView, trackingFragment).commit();
                    }else {
                        tracker.showSettingsAlert();
                    }
//                    String uri = String.format(Locale.ENGLISH, "geo:%f,%f", Double.parseDouble(orderModelArrayList.get(position).getLatitude()), Double.parseDouble(orderModelArrayList.get(position).getLogitude()));
//                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
//                    getActivity().startActivity(intent);
                }
            });
//            Picasso.with(activity)
//                    .load(Url.getInstance().profilePicturesURL + serviceListModels.get(position).getPicturePath())
//                    .placeholder(R.mipmap.logo)
//                    .into(holder.orderImageView);
            holder.orderNameTextView.setText(orderModelArrayList.get(position).getOrderId() +"   "+ orderModelArrayList.get(position).getClientName());
//            if (((Double)Double.parseDouble(serviceListModels.get(position).getDistance())).intValue() > 1000)
//                holder.distanceTextView.setText(activity.getResources().getString(R.string.distance) + " " +  orderModelArrayList.get(position).getDistance() + activity.getResources().getString(R.string.km));
//            else if (Integer.parseInt(serviceListModels.get(position).getDistance()) < 1000)
                holder.distanceTextView.setText(activity.getResources().getString(R.string.distance) + " " + ((Double)Double.parseDouble(orderModelArrayList.get(position).getDistance())).intValue() + " " + activity.getResources().getString(R.string.km));

            return convertView;
        }

        public int getCount() {
            return orderModelArrayList.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        private class ViewHolder {
            private ImageView orderImageView;
            private TextView orderNameTextView;
            private TextView distanceTextView;
            private ImageView locationImageView;
        }
    }

    private void toEditOrderFragment(String getJson){
        EditOrderFragment editOrderFragment = new EditOrderFragment();
        Bundle args = new Bundle();
        Gson gson = new GsonBuilder().create();
        JsonArray orderModelJsonArray = gson.toJsonTree(orderModelArrayList).getAsJsonArray();
        args.putString("orderJson", orderModelJsonArray.get(index).toString());
        args.putString("json", getJson);
        editOrderFragment.setArguments(args);
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.contentView, editOrderFragment).addToBackStack("").commit();
    }

    @Override
    public void onResponse(Object response) {
        Log.e("response", response.toString());
        swipeRefreshListView.setRefreshing(false);
        try {
            if (requestedUrl.equals(Url.getInstance().syncDataURL)){
                if (CheckResponse.getInstance().checkResponse(getActivity(), response.toString(), true)) {
                    toEditOrderFragment(response.toString());
                }
            } else if (requestedUrl.equals(Url.getInstance().orderListURL)){
                if (CheckResponse.getInstance().checkResponse(getActivity(), response.toString(), true)) {
                    decodeJson(response.toString());
                }
            }
        } catch (Exception e){
            Toast.makeText(getActivity(), getResources().getString(R.string.connectionError), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        VolleyLog.d("responseError", "Error: " + error.getMessage());
        try {
            ConnectionVolley.dialog.dismiss();
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
