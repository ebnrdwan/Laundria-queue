package ash.laundriyadriverr.models;

/**
 * Created by ahmed on 2/6/17.
 */

public class OrderModel {

    private String OrderId;
    private String Branch;
    private String DeliveryAddress;
    private String AddressHint;
    private String BuildingType;
    private String FloorNumber;
    private String FlatNumber;
    private String Latitude;
    private String Logitude;
    private String ClientName;
    private String ClientNumber;
    private String Cost;
    private String state;
    private String distance;
    private String OrderDate;
    private Object OrderItems;
    private String DeliveringDate;
    private Object orderitems;

    public String getOrderId() {
        return Integer.toString(Double.valueOf(OrderId).intValue());
    }

    public void setOrderId(String orderId) {
        OrderId = orderId;
    }

    public String getBranch() {
        return Branch;
    }

    public void setBranch(String branch) {
        Branch = branch;
    }

    public String getDeliveryAddress() {
        return DeliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        DeliveryAddress = deliveryAddress;
    }

    public String getAddressHint() {
        return AddressHint;
    }

    public void setAddressHint(String addressHint) {
        AddressHint = addressHint;
    }

    public String getBuildingType() {
        return BuildingType;
    }

    public void setBuildingType(String buildingType) {
        BuildingType = buildingType;
    }

    public String getFloorNumber() {
        return FloorNumber;
    }

    public void setFloorNumber(String floorNumber) {
        FloorNumber = floorNumber;
    }

    public String getFlatNumber() {
        return FlatNumber;
    }

    public void setFlatNumber(String flatNumber) {
        FlatNumber = flatNumber;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLogitude() {
        return Logitude;
    }

    public void setLogitude(String logitude) {
        Logitude = logitude;
    }

    public String getClientName() {
        return ClientName;
    }

    public void setClientName(String clientName) {
        ClientName = clientName;
    }

    public String getClientNumber() {
        return ClientNumber;
    }

    public void setClientNumber(String clientNumber) {
        ClientNumber = clientNumber;
    }

    public String getCost() {
        return Cost;
    }

    public void setCost(String cost) {
        Cost = cost;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getOrderDate() {
        return OrderDate;
    }

    public void setOrderDate(String orderDate) {
        OrderDate = orderDate;
    }

    public Object getOrderItems() {
        return OrderItems;
    }

    public void setOrderItems(Object orderItems) {
        OrderItems = orderItems;
    }

    public String getDeliveringDate() {
        return DeliveringDate;
    }

    public void setDeliveringDate(String deliveringDate) {
        DeliveringDate = deliveringDate;
    }

    public Object getOrderitems() {
        return orderitems;
    }

    public void setOrderitems(Object orderitems) {
        this.orderitems = orderitems;
    }
}
