package ash.laundriyadriverr.models;

/**
 * Created by ahmed on 2/13/17.
 */

public class OrderItemsModel {

    private String Id;
    private String ItemArabicName;
    private String ItemEnglishName;
    private String Amount;
    private String Notes;
    private String serviceId;
    private String TypeId;
    private String price;

    public OrderItemsModel(String id, String amount, String notes, String serviceId, String typeId, String ItemArabicName, String ItemEnglishName) {
        Id = id;
        Amount = amount;
        Notes = notes;
        this.serviceId = serviceId;
        TypeId = typeId;
        this.ItemArabicName = ItemArabicName;
        this.ItemEnglishName = ItemEnglishName;
    }

    public String getId() {
        return Integer.toString(Double.valueOf(Id).intValue());
    }

    public void setId(String id) {
        Id = id;
    }

    public String getAmount() {
        return Integer.toString(Double.valueOf(Amount).intValue());
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getNotes() {
        return Notes;
    }

    public void setNotes(String notes) {
        Notes = notes;
    }

    public String getServiceId() {
        return Integer.toString(Double.valueOf(serviceId).intValue());
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getTypeId() {
        return Integer.toString(Double.valueOf(TypeId).intValue());
    }

    public void setTypeId(String typeId) {
        TypeId = typeId;
    }

    public String getItemArabicName() {
        return ItemArabicName;
    }

    public void setItemArabicName(String itemArabicName) {
        ItemArabicName = itemArabicName;
    }

    public String getItemEnglishName() {
        return ItemEnglishName;
    }

    public void setItemEnglishName(String itemEnglishName) {
        ItemEnglishName = itemEnglishName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
