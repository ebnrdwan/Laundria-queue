package ash.laundriyadriverr.models;

/**
 * Created by ahmed on 2/6/17.
 */

public class LoginModel {

    private String Success;
    private String Token;
    private String Code;
    private String EnglishMessage;
    private String ArabicMessage;

    public String getSuccess() {
        return Success;
    }

    public void setSuccess(String success) {
        Success = success;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getEnglishMessage() {
        return EnglishMessage;
    }

    public void setEnglishMessage(String englishMessage) {
        EnglishMessage = englishMessage;
    }

    public String getArabicMessage() {
        return ArabicMessage;
    }

    public void setArabicMessage(String arabicMessage) {
        ArabicMessage = arabicMessage;
    }
}
