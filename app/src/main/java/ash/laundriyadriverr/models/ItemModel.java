package ash.laundriyadriverr.models;

import ash.laundriyadriverr.utils.GeneralClass;

/**
 * Created by ahmed on 2/13/17.
 */

public class ItemModel {

    private String Id;
    private String EnglishName;
    private String ArabicName;
    private String EnglishDescription;
    private String ArabicDescription;
    private String Category;
    private String Price;
    private String unitpoint;
    private String EstimatedCockingTime;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getEnglishName() {
        return EnglishName;
    }

    public void setEnglishName(String englishName) {
        EnglishName = englishName;
    }

    public String getArabicName() {
        return ArabicName;
    }

    public void setArabicName(String arabicName) {
        ArabicName = arabicName;
    }

    public String getEnglishDescription() {
        return EnglishDescription;
    }

    public void setEnglishDescription(String englishDescription) {
        EnglishDescription = englishDescription;
    }

    public String getArabicDescription() {
        return ArabicDescription;
    }

    public void setArabicDescription(String arabicDescription) {
        ArabicDescription = arabicDescription;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getUnitpoint() {
        return unitpoint;
    }

    public void setUnitpoint(String unitpoint) {
        this.unitpoint = unitpoint;
    }

    public String getEstimatedCockingTime() {
        return EstimatedCockingTime;
    }

    public void setEstimatedCockingTime(String estimatedCockingTime) {
        EstimatedCockingTime = estimatedCockingTime;
    }

    public String toString(){
        if(GeneralClass.language.equals("en"))
            return getEnglishName();
        else
            return getArabicName();
    }
}
