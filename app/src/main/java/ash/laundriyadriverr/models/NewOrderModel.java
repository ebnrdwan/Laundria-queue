package ash.laundriyadriverr.models;

/**
 * Created by ahmed on 3/22/17.
 */

public class NewOrderModel {

    private String Orderid;
    private String Name;
    private String MobileNumber;
    private String distance;
    CategoryModel[] Category;
    //private CategoryModel Category;
    private String Lat;
    private String Lng;
    private String type;

    public NewOrderModel(String orderid, String name, String mobileNumber, String distance, CategoryModel [] category, String lat, String lng, String type) {
        Orderid = orderid;
        Name = name;
        MobileNumber = mobileNumber;
        this.distance = distance;
        Category = category;
        Lat = lat;
        Lng = lng;
        this.type = type;
    }

    public CategoryModel[] getCategory() {
        return Category;
    }

    public void setCategory(CategoryModel[] category) {
        Category = category;
    }

    public String getOrderid() {
        return Orderid;
    }

    public void setOrderid(String orderid) {
        Orderid = orderid;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }



    public String getLat() {
        return Lat;
    }

    public void setLat(String lat) {
        Lat = lat;
    }

    public String getLng() {
        return Lng;
    }

    public void setLng(String lng) {
        Lng = lng;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
