package ash.laundriyadriverr.models;

import ash.laundriyadriverr.utils.GeneralClass;

/**
 * Created by ahmed on 2/13/17.
 */

public class ServiceTypeModel {

    private String Id;
    private String EnglishName;
    private String ArabicName;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getEnglishName() {
        return EnglishName;
    }

    public void setEnglishName(String englishName) {
        EnglishName = englishName;
    }

    public String getArabicName() {
        return ArabicName;
    }

    public void setArabicName(String arabicName) {
        ArabicName = arabicName;
    }

    public String toString(){
        if(GeneralClass.language.equals("en"))
            return getEnglishName();
        else
            return getArabicName();
    }
}
