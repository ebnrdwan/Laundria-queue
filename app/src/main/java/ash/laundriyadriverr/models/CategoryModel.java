package ash.laundriyadriverr.models;

import ash.laundriyadriverr.utils.GeneralClass;

/**
 * Created by ahmed on 2/13/17.
 */

public class CategoryModel {

    private String Company;
    private String Items;
    private String ParentCategoryId;
    private String EnglishName;
    private String ArabicName;
    private String ParentCategory;
    private boolean EnableNow;
    private boolean DirectToAdmin;
    private int MinCharge;
    private int Id;
    private int CompanyId;
    private boolean IsDeleted;

    public CategoryModel(String company, String items, String parentCategoryId, String englishName, String arabicName, String parentCategory, boolean enableNow, boolean directToAdmin, int minCharge, int id, int companyId, boolean isDeleted) {
        Company = company;
        Items = items;
        ParentCategoryId = parentCategoryId;
        EnglishName = englishName;
        ArabicName = arabicName;
        ParentCategory = parentCategory;
        EnableNow = enableNow;
        DirectToAdmin = directToAdmin;
        MinCharge = minCharge;
        Id = id;
        CompanyId = companyId;
        IsDeleted = isDeleted;
    }

//    public CategoryModel() {
//    }

    public String getCompany() {
        return Company;
    }

    public void setCompany(String company) {
        Company = company;
    }

    public String getItems() {
        return Items;
    }

    public void setItems(String items) {
        Items = items;
    }

    public String getParentCategoryId() {
        return ParentCategoryId;
    }

    public void setParentCategoryId(String parentCategoryId) {
        ParentCategoryId = parentCategoryId;
    }

    public String getEnglishName() {
        return EnglishName;
    }

    public void setEnglishName(String englishName) {
        EnglishName = englishName;
    }

    public String getArabicName() {
        return ArabicName;
    }

    public void setArabicName(String arabicName) {
        ArabicName = arabicName;
    }

    public String getParentCategory() {
        return ParentCategory;
    }

    public void setParentCategory(String parentCategory) {
        ParentCategory = parentCategory;
    }

    public boolean isEnableNow() {
        return EnableNow;
    }

    public void setEnableNow(boolean enableNow) {
        EnableNow = enableNow;
    }

    public boolean isDirectToAdmin() {
        return DirectToAdmin;
    }

    public void setDirectToAdmin(boolean directToAdmin) {
        DirectToAdmin = directToAdmin;
    }

    public int getMinCharge() {
        return MinCharge;
    }

    public void setMinCharge(int minCharge) {
        MinCharge = minCharge;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getCompanyId() {
        return CompanyId;
    }

    public void setCompanyId(int companyId) {
        CompanyId = companyId;
    }

    public boolean isDeleted() {
        return IsDeleted;
    }

    public void setDeleted(boolean deleted) {
        IsDeleted = deleted;
    }

    public String toString(){
        if(GeneralClass.language.equals("en"))
            return getEnglishName();
        else
            return getArabicName();
    }
}
