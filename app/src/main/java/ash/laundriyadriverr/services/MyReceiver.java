package ash.laundriyadriverr.services;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import ash.laundriyadriverr.R;
import ash.laundriyadriverr.activities.NewOrderActivity;


public class MyReceiver extends BroadcastReceiver {

    public MyReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.

        Intent i = new Intent(context, NewOrderActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Bundle extras = new Bundle();
        extras.putString("json", intent.getStringExtra("json"));
        i.putExtras(extras);
        context.startActivity(i);


//        Dialog dialog = new Dialog(context);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.progress_dialog);
//        dialog.setCancelable(false);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        dialog.show();

        //addDialog(context);
    }

    private void addDialog(Context context){
        final Dialog dialog = new Dialog(context.getApplicationContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.items_dialog);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Button addButton = (Button)  dialog.findViewById(R.id.addButton);
        final Spinner itemSpinner = (Spinner)  dialog.findViewById(R.id.itemSpinner);
        final Spinner serviceSpinner = (Spinner)  dialog.findViewById(R.id.serviceSpinner);
        final Spinner serviceTypeSpinner = (Spinner)  dialog.findViewById(R.id.serviceTypeSpinner);
        final Spinner categorySpinner = (Spinner) dialog.findViewById(R.id.categorySpinner);
        TextView plusButton = (TextView) dialog.findViewById(R.id.plusButton);
        TextView minusButton = (TextView) dialog.findViewById(R.id.minusButton);
        final EditText counterEditText = (EditText) dialog.findViewById(R.id.counterEditText);

        plusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                counterEditText.setText(Integer.parseInt(counterEditText.getText().toString()) + 1 + "");
            }
        });

        minusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!counterEditText.getText().toString().equals("1")){
                    counterEditText.setText(Integer.parseInt(counterEditText.getText().toString()) - 1 + "");
                }
            }
        });

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
